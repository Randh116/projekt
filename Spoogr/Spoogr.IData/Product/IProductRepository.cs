﻿using System;
using System.Threading.Tasks;

namespace Spoogr.IData.Product
{
    public interface IProductRepository
    {
        Task<int> AddProduct(Spoogr.Domain.Product.Product product);
        Task<Spoogr.Domain.Product.Product> GetProduct(int productId);
        Task<Spoogr.Domain.Product.Product> GetProduct(string productName);
        Task EditProduct(Domain.Product.Product product);
        Task DeleteProduct(int productId);
    }
}