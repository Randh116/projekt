﻿using System;
using System.Threading.Tasks;

namespace Spoogr.IData.User
{
    public interface IUserRepository
    {
        Task<int> AddUser(Spoogr.Domain.User.User user);
        Task<Spoogr.Domain.User.User> GetUser(int userId);
        Task<Spoogr.Domain.User.User> GetUser(string userName);
        Task EditUser(Domain.User.User user);
        Task DeleteUser(int userId);
    }
}