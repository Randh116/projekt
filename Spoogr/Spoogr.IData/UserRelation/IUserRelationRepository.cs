﻿using System;
using System.Threading.Tasks;

namespace Spoogr.IData.UserRelation
{
    public interface IUserRelationRepository
    {
        Task<int> AddUserRelation(Spoogr.Domain.UserRelation.UserRelation userRelation);
        Task<Spoogr.Domain.UserRelation.UserRelation> GetUserRelation(int userRelationId);
        Task DeleteUserRelation(int userRelationId);
    }
}