﻿using System;
using System.Threading.Tasks;

namespace Spoogr.IData.PreparedProduct
{
    public interface IPreparedProductRepository
    {
        Task<int> AddPreparedProduct(Spoogr.Domain.PreparedProduct.PreparedProduct preparedProduct);
        Task<Spoogr.Domain.PreparedProduct.PreparedProduct> GetPreparedProduct(int preparedProductId);
        Task DeletePreparedProduct(int preparedProductId);
    }
}