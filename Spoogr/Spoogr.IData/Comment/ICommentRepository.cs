﻿using System;
using System.Threading.Tasks;

namespace Spoogr.IData.Comment
{
    public interface ICommentRepository
    {
        Task<int> AddComment(Spoogr.Domain.Comment.Comment comment);
        Task<Spoogr.Domain.Comment.Comment> GetComment(int commentId);
        Task EditComment(Domain.Comment.Comment comment);
        Task DeleteComment(int commentId);
    }
}