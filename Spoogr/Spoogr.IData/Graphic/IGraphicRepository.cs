﻿using System;
using System.Threading.Tasks;

// 1. domain.object.object.cs
// 2. idata.object.iobjectrepository.cs
// 3. data.sql.object.objectrepository.cs
// 4. zmiana wszystkich obiektow w DAOConfigutations i DatabaseSeed na DAO.Object
// 5. iservices.requests.createobject, .editobject
// 6. iservices.object.iobjectservice
// 7. services.object.objectservice
// 8. api.viewmodels.objectviewmodel
// 9. api.mappers.objecttoobjectmodelmapper
// 10. api.validation.createobjectvalidator, .editobjectvalidator
// 11. api.controllers.objectcontroller
// 12. api.bindingmodels.createobject, .editobject


namespace Spoogr.IData.Graphic
{
    public interface IGraphicRepository
    {
        Task<int> AddGraphic(Spoogr.Domain.Graphic.Graphic graphic);
        Task<Spoogr.Domain.Graphic.Graphic> GetGraphic(int graphicId);
        Task EditGraphic(Domain.Graphic.Graphic graphic);
        Task DeleteGraphic(int graphicId);
    }
}