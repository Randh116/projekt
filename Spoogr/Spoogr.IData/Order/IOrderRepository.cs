﻿using System;
using System.Threading.Tasks;

namespace Spoogr.IData.Order
{
    // dodac editorder i zmodyfikowac tabele
    public interface IOrderRepository
    {
        Task<int> AddOrder(Spoogr.Domain.Order.Order order);
        Task<Spoogr.Domain.Order.Order> GetOrder(int orderId);
        Task DeleteOrder(int orderId);
    }
}