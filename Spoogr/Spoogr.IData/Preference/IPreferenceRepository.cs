﻿using System;
using System.Threading.Tasks;

namespace Spoogr.IData.Preference
{
    public interface IPreferenceRepository
    {
        Task<int> AddPreference(Spoogr.Domain.Preference.Preference preference);
        Task<Spoogr.Domain.Preference.Preference> GetPreference(int preferenceId);
        Task EditPreference(Domain.Preference.Preference preference);
        Task DeletePreference(int preferenceId);
    }
}