﻿using System;
using System.Threading.Tasks;

namespace Spoogr.IData.ProductCategory
{
    public interface IProductCategoryRepository
    {
        Task<int> AddProductCategory(Spoogr.Domain.ProductCategory.ProductCategory productCategory);
        Task<Spoogr.Domain.ProductCategory.ProductCategory> GetProductCategory(int productCategoryId);
        Task DeleteProductCategory(int productCategoryId);
    }
}