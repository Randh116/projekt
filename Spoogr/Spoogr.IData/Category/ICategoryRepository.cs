﻿using System;
using System.Threading.Tasks;

namespace Spoogr.IData.Category
{
    public interface ICategoryRepository
    {
        Task<int> AddCategory(Spoogr.Domain.Category.Category category);
        Task<Spoogr.Domain.Category.Category> GetCategory(int categoryId);
        Task<Spoogr.Domain.Category.Category> GetCategory(string categoryName);
        Task EditCategory(Domain.Category.Category category);
        Task DeleteCategory(int categoryId);
    }
}