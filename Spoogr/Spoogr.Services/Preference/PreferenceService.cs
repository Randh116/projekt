﻿using System.ComponentModel;
using System.Threading.Tasks;
using Spoogr.IData.Preference;
using Spoogr.IServices.Requests;
using Spoogr.IServices.Preference;

namespace Spoogr.Services.Preference
{
    public class PreferenceService: IPreferenceService
    {
        private readonly IPreferenceRepository _preferenceRepository;

        public PreferenceService(IPreferenceRepository preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }

        public Task<Domain.Preference.Preference> GetPreferenceByPreferenceId(int preferenceId)
        {
            return _preferenceRepository.GetPreference(preferenceId);
        }

        public async Task<Domain.Preference.Preference> CreatePreference(CreatePreference createPreference)
        {
            var preference = new Domain.Preference.Preference(createPreference.UserId, createPreference.GraphicId);
            preference.PreferenceId = await _preferenceRepository.AddPreference(preference);
            return preference;
        }

        // nwm tu moze ma byc edit ale musze potem sprawdzic
        
        public async Task DeletePreference(int preferenceId)
        {
            await _preferenceRepository.DeletePreference(preferenceId);
        }
    }
}