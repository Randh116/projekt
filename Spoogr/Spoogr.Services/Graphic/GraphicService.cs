﻿using System.ComponentModel;
using System.Threading.Tasks;
using Spoogr.IData.Graphic;
using Spoogr.IServices.Graphic;
using Spoogr.IServices.Requests;
using Spoogr.IServices.Product;

namespace Spoogr.Services.Graphic
{
    public class GraphicService: IGraphicService
    {
        private readonly IGraphicRepository _graphicRepository;

        public GraphicService(IGraphicRepository graphicRepository)
        {
            _graphicRepository = graphicRepository;
        }

        public Task<Domain.Graphic.Graphic> GetGraphicByGraphicId(int graphicId)
        {
            return _graphicRepository.GetGraphic(graphicId);
        }

        public async Task<Domain.Graphic.Graphic> CreateGraphic(CreateGraphic createGraphic)
        {
            var graphic = new Domain.Graphic.Graphic(createGraphic.UserId, createGraphic.GraphicDescription,
                createGraphic.Price);
            graphic.GraphicId = await _graphicRepository.AddGraphic(graphic);
            return graphic;
        }

        public async Task EditGraphic(EditGraphic createGraphic, int graphicId)
        {
            var graphic = await _graphicRepository.GetGraphic(graphicId);
            graphic.EditGraphic(createGraphic.GraphicDescription, createGraphic.GraphicHref, createGraphic.Price);
            await _graphicRepository.EditGraphic(graphic);
        }

        public async Task DeleteGraphic(int graphicId)
        {
            await _graphicRepository.DeleteGraphic(graphicId);
        }
    }
}