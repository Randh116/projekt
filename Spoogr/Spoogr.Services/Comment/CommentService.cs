﻿using System.ComponentModel;
using System.Threading.Tasks;
using Spoogr.IData.Comment;
using Spoogr.IServices.Requests;
using Spoogr.IServices.Comment;

namespace Spoogr.Services.Comment
{
    public class CommentService: ICommentService
    {
        private readonly ICommentRepository _commentRepository;

        public CommentService(ICommentRepository commentRepository)
        {
            _commentRepository = commentRepository;
        }

        public Task<Domain.Comment.Comment> GetCommentByCommentId(int commentId)
        {
            return _commentRepository.GetComment(commentId);
        }

        public async Task<Domain.Comment.Comment> CreateComment(CreateComment createComment)
        {
            var comment = new Domain.Comment.Comment(createComment.GraphicId, createComment.UserId);
            comment.CommentId = await _commentRepository.AddComment(comment);
            return comment;
        }

        public async Task EditComment(EditComment createComment, int commentId)
        {
            var comment = await _commentRepository.GetComment(commentId);
            comment.EditComment(createComment.CommentBody);
            await _commentRepository.EditComment(comment);
        }

        public async Task DeleteComment(int commentId)
        {
            await _commentRepository.DeleteComment(commentId);
        }
    }
}