﻿using System.ComponentModel;
using System.Threading.Tasks;
using Spoogr.IData.Product;
using Spoogr.IServices.Requests;
using Spoogr.IServices.Product;

namespace Spoogr.Services.Product
{
    public class ProductService: IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public Task<Domain.Product.Product> GetProductByProductId(int productId)
        {
            return _productRepository.GetProduct(productId);
        }

        public Task<Domain.Product.Product> GetProductByProductName(string productName)
        {
            return _productRepository.GetProduct(productName);
        }

        public async Task<Domain.Product.Product> CreateProduct(CreateProduct createProduct)
        {
            var product = new Domain.Product.Product(createProduct.UserId, createProduct.ProductName, createProduct.Price);
            product.ProductId = await _productRepository.AddProduct(product);
            return product;
        }

        public async Task EditProduct(EditProduct createProduct, int productId)
        {
            var product = await _productRepository.GetProduct(productId);
            product.EditProduct(createProduct.ProductName, createProduct.ProductDescription, createProduct.ProductHref, createProduct.Price);
            await _productRepository.EditProduct(product);
        }

        public async Task DeleteProduct(int productId)
        {
            await _productRepository.DeleteProduct(productId);
        }
    }
}