﻿using System.ComponentModel;
using System.Threading.Tasks;
using Spoogr.IData.Category;
using Spoogr.IServices.Requests;
using Spoogr.IServices.Category;

namespace Spoogr.Services.Category
{
    public class CategoryService: ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryService(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public Task<Domain.Category.Category> GetCategoryByCategoryId(int categoryId)
        {
            return _categoryRepository.GetCategory(categoryId);
        }

        public Task<Domain.Category.Category> GetCategoryByCategoryName(string categoryName)
        {
            return _categoryRepository.GetCategory(categoryName);
        }

        public async Task<Domain.Category.Category> CreateCategory(CreateCategory createCategory)
        {
            var category = new Domain.Category.Category(createCategory.CategoryName);
            category.CategoryId = await _categoryRepository.AddCategory(category);
            return category;
        }

        public async Task EditCategory(EditCategory createCategory, int categoryId)
        {
            var category = await _categoryRepository.GetCategory(categoryId);
            category.EditCategory(createCategory.CategoryName, createCategory.ImageHref);
            await _categoryRepository.EditCategory(category);
        }

        public async Task DeleteCategory(int categoryId)
        {
            await _categoryRepository.DeleteCategory(categoryId);
        }
    }
}