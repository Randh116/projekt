﻿using System.ComponentModel;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Spoogr.IData.PreparedProduct;
using Spoogr.IServices.Requests;
using Spoogr.IServices.PreparedProduct;

namespace Spoogr.Services.PreparedProduct
{
    public class PreparedProductService: IPreparedProductService
    {
        private readonly IPreparedProductRepository _preparedProductRepository;

        public PreparedProductService(IPreparedProductRepository preparedProductRepository)
        {
            _preparedProductRepository = preparedProductRepository;
        }

        public Task<Domain.PreparedProduct.PreparedProduct> GetPreparedProductByPreparedProductId(int preparedProductId)
        {
            return _preparedProductRepository.GetPreparedProduct(preparedProductId);
        }

        public async Task<Domain.PreparedProduct.PreparedProduct> CreatePreparedProduct(CreatePreparedProduct createPreparedProduct)
        {
            var preparedProduct = new Domain.PreparedProduct.PreparedProduct(
                createPreparedProduct.UserId, createPreparedProduct.GraphicId, 
                createPreparedProduct.ProductId, createPreparedProduct.OrderId, 
                createPreparedProduct.Price);
            preparedProduct.PreparedProductId = await _preparedProductRepository.AddPreparedProduct(preparedProduct);
            return preparedProduct;
        }

        public async Task DeletePreparedProduct(int preparedProductId)
        {
            await _preparedProductRepository.DeletePreparedProduct(preparedProductId);
        }
    }
}