﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Spoogr.IServices.Requests;
using System.ComponentModel;
using Spoogr.IServices.Order;
using Spoogr.IData.Order;

namespace Spoogr.Services.Order
{
    public class OrderService: IOrderService
    {
        private readonly IOrderRepository _orderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public Task<Domain.Order.Order> GetOrderByOrderId(int orderId)
        {
            return _orderRepository.GetOrder(orderId);
        }

        public async Task<Domain.Order.Order> CreateOrder(CreateOrder createOrder)
        {
            var order = new Domain.Order.Order(createOrder.UserId);
            order.OrderId = await _orderRepository.AddOrder(order);
            return order;
        }

        public async Task DeleteOrder(int orderId)
        {
            await _orderRepository.DeleteOrder(orderId);
        }
    }
}