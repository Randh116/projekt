using System;
using System.Threading.Tasks;
using Spoogr.Common.Enums;
using Spoogr.Domain.DomainExceptions;
using Spoogr.IData.User;
using Spoogr.IServices.Requests;
using Spoogr.IServices.User;
using Spoogr.Services.User;
using Moq;
using NUnit;
using NUnit.Framework;


namespace Spoogr.Services.Tests.NUnit
{
    public class UserServiceTestNUnit
    {
        private readonly IUserService _userService;
        private readonly Mock<IUserRepository> _userRepositoryMock;

        public UserServiceTestNUnit()
        {
            _userRepositoryMock = new Mock<IUserRepository>();
            _userService = new UserService(_userRepositoryMock.Object);
        }

        [Test]
        public void CreateUser_Returns_Throws_InvalidBirthDateException_NUnit()
        {
            var user = new CreateUser
            {
                Email = "Email",
                UserName = "Name",
                Gender = Gender.Male,
                BirthDate = DateTime.UtcNow.AddHours(1)
            };
        }

        [Test]
        public async Task CreateUser_Returns_Correct_Response_NUnit()
        {
            var user = new CreateUser
            {
                Email = "Email",
                UserName = "Name",
                Gender = Gender.Male,
                BirthDate = DateTime.UtcNow,
            };

            int expectedResult = 0;
            _userRepositoryMock.Setup(x => x.AddUser(
                    new Domain.User.User(
                        user.UserName,
                        user.Email,
                        user.Gender,
                        user.BirthDate)))
                .Returns(Task.FromResult(expectedResult));

            var result = await _userService.CreateUser(user);
            
            Assert.IsInstanceOf<Domain.User.User>(result);
            Assert.NotNull(result);
            Assert.AreEqual(expectedResult, result.UserId);
        }
    }
}