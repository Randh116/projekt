using FluentValidation;
using FluentValidation.AspNetCore;
using Spoogr.Api.Middlewares;
using Spoogr.Data.Sql;
using Spoogr.Data.Sql.Migrations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Spoogr.Api.Validation;
using Spoogr.Data.Sql;
using Spoogr.Data.Sql.Migrations;
using Spoogr.Data.Sql.Product;
using Spoogr.Data.Sql.User;
using Spoogr.Data.Sql.Graphic;
using Spoogr.IData.Product;
using Spoogr.IData.User;
using Spoogr.IData.Graphic;
using Spoogr.IServices.Product;
using Spoogr.IServices.Requests;
using Spoogr.IServices.User;
using Spoogr.IServices.Graphic;
using Spoogr.Services.Product;
using Spoogr.Services.User;
using Spoogr.Services.Graphic;
using CreateUser = Spoogr.IServices.Requests.CreateUser;
using CreateProduct = Spoogr.IServices.Requests.CreateProduct;
using CreateGraphic = Spoogr.IServices.Requests.CreateGraphic;
using EditProduct = Spoogr.Api.BindingModels.EditProduct;
using EditProductValidator = Spoogr.Api.BindingModels.EditProductValidator;
using EditUser = Spoogr.Api.BindingModels.EditUser;
using EditUserValidator = Spoogr.Api.BindingModels.EditUserValidator;

// pododawac do konfiguracji wszystkie pozostale tabele

namespace Spoogr.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<SpoogrDbContext>(options => options
                .UseNpgsql(Configuration.GetConnectionString("SpoogrDbContext")));
            services.AddTransient<DatabaseSeed>();
            services.AddControllers()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                })
                .AddFluentValidation();
            
            services.AddTransient<IValidator<EditUser>, EditUserValidator>();
            services.AddTransient<IValidator<CreateUser>, CreateUserValidator>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserRepository, UserRepository>();
            
            services.AddTransient<IValidator<EditProduct>, EditProductValidator>();
            services.AddTransient<IValidator<CreateProduct>, CreateProductValidator>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IProductRepository, ProductRepository>();

            services.AddApiVersioning(o => o.ReportApiVersions = true);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<SpoogrDbContext>();
                var databaseSeed = serviceScope.ServiceProvider.GetRequiredService<DatabaseSeed>();
                
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
                databaseSeed.Seed();
            }

            app.UseMiddleware<ErrorHandlerMiddleware>();
            app.UseRouting();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

        }
    }
}