﻿using System;
using System.ComponentModel.DataAnnotations;
using Spoogr.Common.Enums;

namespace Spoogr.Api.BindingModels
{
    public class CreateUser
    {
        [Required]
        [Display(Name = "Username")]
        public string UserName { get; set; }
        
        [Required]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }
        
        [Required]
        [Display(Name = "Gender")]
        public Gender Gender { get; set; }
        
        [Required]
        [Display(Name = "BirthDate")]
        public DateTime BirthDate { get; set; }
        
    }
}