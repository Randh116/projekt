﻿using System;
using System.ComponentModel.DataAnnotations;
using FluentValidation;
using Spoogr.Common.Enums;

namespace Spoogr.Api.BindingModels
{
    public class EditProduct
    {
        [Display(Name = "ProductName")]
        public string ProductName { get; set; }
        
        [Display(Name = "ProductDescription")]
        public string ProductDescription { get; set; }

        [Display(Name = "ProductHref")]
        public string ProductHref { get; set; }
        
        [Display(Name = "Price")]
        public int Price { get; set; }
    }

    public class EditProductValidator : AbstractValidator<EditProduct>
    {
        public EditProductValidator()
        {
            RuleFor(x => x.ProductName).NotNull();
            RuleFor(x => x.ProductDescription).NotNull();
            RuleFor(x => x.ProductHref).NotNull();
            RuleFor(x => x.Price).NotNull();
        }
    }
}