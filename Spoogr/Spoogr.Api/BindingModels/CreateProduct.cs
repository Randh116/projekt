﻿using System;
using System.ComponentModel.DataAnnotations;
using Spoogr.Common.Enums;

namespace Spoogr.Api.BindingModels
{
    public class CreateProduct
    {
        [Required]
        [Display(Name = "ProductName")]
        public string ProductName { get; set; }

        [Required]
        [Display(Name = "ProductDescription")]
        public string ProductDescription { get; set; }
        
        [Required]
        [Display(Name = "ProductHref")]
        public string ProductHref { get; set; }
        
        [Required]
        [Display(Name = "Price")]
        public int Price { get; set; }
        
        [Required]
        [Display(Name = "UserId")]
        public int UserId { get; set; }
    }
}