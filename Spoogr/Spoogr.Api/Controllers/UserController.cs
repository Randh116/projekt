﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Features;
using Spoogr.Api.BindingModels;
using Spoogr.Api.Validation;
using Spoogr.Api.ViewModels;
using Spoogr.Data.Sql;
using Spoogr.Data.Sql.DAO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Spoogr.Api.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/user")]
    
    public class UserController : Controller
    {
        private readonly SpoogrDbContext _context;

        public UserController(SpoogrDbContext context)
        {
            _context = context;
        }

        [HttpGet("{userId:min(1)}", Name = "GetUserById")]
        public async Task<IActionResult> GetUserById(int userId)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.UserId == userId);

            if (user != null)
            {
                return Ok(new UserViewModel
                {
                    UserId = user.UserId,
                    Email = user.Email,
                    Gender = user.Gender,
                    UserName = user.UserName,
                    BirthDate = user.BirthDate,
                    EditionDate = user.EditionDate,
                    RegistrationDate = user.RegistrationDate,
                    AccountDescription = user.AccountDescription,
                    FollowersCount = user.FollowersCount,
                    FollowingCount = user.FollowingCount,
                    IsActiveUser = user.IsActiveUser,
                    IsBannedUser = user.IsBannedUser,
                    IconHref = user.IconHref,
                    GraphicsCount = user.GraphicsCount
                });
            }

            return NotFound();
        }

        [HttpGet("name/{userName}", Name = "GetUserByUserName")]
        public async Task<IActionResult> GetUserByUserName(string userName)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.UserName == userName);

            if (user != null)
            {
                return Ok(new UserViewModel
                {
                    UserId = user.UserId,
                    Email = user.Email,
                    Gender = user.Gender,
                    UserName = user.UserName,
                    BirthDate = user.BirthDate,
                    EditionDate = user.EditionDate,
                    RegistrationDate = user.RegistrationDate,
                    AccountDescription = user.AccountDescription,
                    FollowersCount = user.FollowersCount,
                    FollowingCount = user.FollowingCount,
                    IsActiveUser = user.IsActiveUser,
                    IsBannedUser = user.IsBannedUser,
                    IconHref = user.IconHref,
                    GraphicsCount = user.GraphicsCount
                });
            }

            return NotFound();
        }

        [ValidateModel]
        public async Task<IActionResult> Post([FromBody] CreateUser createUser)
        {
            var user = new User()
            {
                Email = createUser.Email,
                UserName = createUser.UserName,
                Gender = createUser.Gender,
                BirthDate = createUser.BirthDate,
                RegistrationDate = DateTime.UtcNow,
                EditionDate = DateTime.UtcNow,
                IsActiveUser = true,
                IsBannedUser = false
            };
            await _context.AddAsync(user);
            await _context.SaveChangesAsync();

            return Created(user.UserId.ToString(), new UserViewModel
            {
                UserId = user.UserId,
                Email = user.Email,
                Gender = user.Gender,
                UserName = user.UserName,
                BirthDate = user.BirthDate,
                EditionDate = user.EditionDate,
                RegistrationDate = user.RegistrationDate,
                AccountDescription = user.AccountDescription,
                FollowersCount = user.FollowersCount,
                FollowingCount = user.FollowingCount,
                IsActiveUser = user.IsActiveUser,
                IsBannedUser = user.IsBannedUser,
                IconHref = user.IconHref,
                GraphicsCount = user.GraphicsCount
            });
        }

        [ValidateModel]
        [HttpPatch("edit/{userId:min(1)}", Name = "EditUser")]
        public async Task<IActionResult> EditUser([FromBody] EditUser editUser, int userId)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.UserId == userId);
            user.UserName = editUser.UserName;
            user.Email = editUser.Email;
            user.BirthDate = editUser.BirthDate;
            user.Gender = editUser.Gender;
            await _context.SaveChangesAsync();
            return NoContent();
        }

        [ValidateModel]
        [HttpPost("delete/{userId:min(1)}", Name = "DeleteUser")]
        public async Task<IActionResult> DeleteUser(int userId)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.UserId == userId);

            _context.User.Remove(user);
            await _context.SaveChangesAsync();
            return NoContent();

        }
    }
}