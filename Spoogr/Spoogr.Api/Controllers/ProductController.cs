﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Features;
using Spoogr.Api.BindingModels;
using Spoogr.Api.Validation;
using Spoogr.Api.ViewModels;
using Spoogr.Data.Sql;
using Spoogr.Data.Sql.DAO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Spoogr.Api.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    
    public class ProductController : Controller
    {
        private readonly SpoogrDbContext _context;

        public ProductController(SpoogrDbContext context)
        {
            _context = context;
        }

        [HttpGet("{productId:min(1)}", Name = "GetProductById")]
        public async Task<IActionResult> GetProductById(int productId)
        {
            var product = await _context.Product.FirstOrDefaultAsync(x => x.ProductId == productId);

            if (product != null)
            {
                return Ok(new ProductViewModel
                {
                    ProductId = product.ProductId,
                    CommentsCount = product.CommentsCount,
                    CreationDate = product.CreationDate,
                    IsActiveProduct = product.IsActiveProduct,
                    IsBannedProduct = product.IsBannedProduct,
                    Price = product.Price,
                    ProductDescription = product.ProductDescription,
                    ProductHref = product.ProductHref,
                    ProductName = product.ProductName,
                    UserId = product.UserId
                });
            }

            return NotFound();
        }

        [HttpGet("name/{productName}", Name = "GetProductByName")]
        public async Task<IActionResult> GetProductByName(string productName)
        {
            var product = await _context.Product.FirstOrDefaultAsync(x => x.ProductName == productName);

            if (product != null)
            {
                return Ok(new ProductViewModel
                {
                    ProductId = product.ProductId,
                    CommentsCount = product.CommentsCount,
                    CreationDate = product.CreationDate,
                    IsActiveProduct = product.IsActiveProduct,
                    IsBannedProduct = product.IsBannedProduct,
                    Price = product.Price,
                    ProductDescription = product.ProductDescription,
                    ProductHref = product.ProductHref,
                    ProductName = product.ProductName,
                    UserId = product.UserId
                });
            }

            return NotFound();
        }

        [ValidateModel]
        public async Task<IActionResult> Post([FromBody] CreateProduct createProduct)
        {
            var product = new Product()
            {
                CreationDate = DateTime.UtcNow,
                IsActiveProduct = true,
                IsBannedProduct = false,
                Price = createProduct.Price,
                ProductDescription = createProduct.ProductDescription,
                ProductHref = createProduct.ProductHref,
                ProductName = createProduct.ProductName,
                UserId = createProduct.UserId
            };
            await _context.AddAsync(product);
            await _context.SaveChangesAsync();

            return Created(product.ProductId.ToString(), new ProductViewModel
            {
                ProductId = product.ProductId,
                CommentsCount = product.CommentsCount,
                CreationDate = product.CreationDate,
                IsActiveProduct = product.IsActiveProduct,
                IsBannedProduct = product.IsBannedProduct,
                Price = product.Price,
                ProductDescription = product.ProductDescription,
                ProductHref = product.ProductHref,
                ProductName = product.ProductName,
                UserId = product.UserId
            });
        }

        [ValidateModel]
        [HttpPatch("edit/{productId:min(1)}", Name = "EditProduct")]
        public async Task<IActionResult> EditProduct([FromBody] EditProduct editProduct, int productId)
        {
            var product = await _context.Product.FirstOrDefaultAsync(x => x.ProductId == productId);
            product.ProductName = editProduct.ProductName;
            product.ProductDescription = editProduct.ProductDescription;
            product.ProductHref = editProduct.ProductHref;
            product.Price = editProduct.Price;
            await _context.SaveChangesAsync();
            return NoContent();
        }

        [ValidateModel]
        [HttpPost("delete/{productId:min(1)}", Name = "DeleteProduct")]
        public async Task<IActionResult> DeleteProduct(int productId)
        {
            var product = await _context.Product.FirstOrDefaultAsync(x => x.ProductId == productId);

            _context.Product.Remove(product);
            await _context.SaveChangesAsync();
            return NoContent();
        }
    }
}