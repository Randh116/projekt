﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Spoogr.Api.Mappers;
using Spoogr.Api.Validation;
using Spoogr.Data.Sql;
using Spoogr.IServices.Product;
using Microsoft.AspNetCore.Mvc;

namespace Spoogr.Api.Controllers
{
    [ApiVersion("2.0")]
    [Route("api/v{version:apiVersion}/product")]
    
    public class ProductV2Controller : Controller
    {
        private readonly SpoogrDbContext _context;
        private readonly IProductService _productService;

        public ProductV2Controller(SpoogrDbContext context, IProductService productService)
        {
            _context = context;
            _productService = productService;
        }

        [HttpGet("{productId:min(1)}", Name = "GetProductByProductId")]
        public async Task<IActionResult> GetProductByProductId(int productId)
        {
            var product = await _productService.GetProductByProductId(productId);
            if (product != null)
            {
                return Ok(ProductToProductViewModelMapper.ProductToProductViewModel(product));
            }

            return NotFound();
        }

        [HttpGet("name/{productName}", Name = "GetProductByProductName")]
        public async Task<IActionResult> GetProductByProductName(string productName)
        {
            var product = await _productService.GetProductByProductName(productName);
            if (product != null)
            {
                return Ok(ProductToProductViewModelMapper.ProductToProductViewModel(product));
            }

            return NotFound();
        }

        [ValidateModel]
        public async Task<IActionResult> Post([FromBody] IServices.Requests.CreateProduct createProduct)
        {
            var product = await _productService.CreateProduct(createProduct);
            
            return Created(product.ProductId.ToString(), ProductToProductViewModelMapper.ProductToProductViewModel(product));
        }

        [ValidateModel]
        [HttpPatch("edit/{productId:min(1)}", Name = "EditProduct")]
        public async Task<IActionResult> EditProduct([FromBody] IServices.Requests.EditProduct editProduct,
            int productId)
        {
            await _productService.EditProduct(editProduct, productId);

            return NoContent();
        }

        [ValidateModel]
        [HttpPost("delete/{productId:min(1)}", Name = "DeleteProduct")]
        public async Task<IActionResult> DeleteProduct(int productId)
        {
            await _productService.DeleteProduct(productId);

            return NoContent();
        }
    }
}