﻿using Spoogr.Api.ViewModels;
using Spoogr.Data.Sql.DAO;

namespace Spoogr.Api.Mappers
{
    public class GraphicToGraphicViewModelMapper
    {
        public static GraphicViewModel GraphicToGraphicViewModel(Domain.Graphic.Graphic graphic)
        {
            var graphicViewModel = new GraphicViewModel
            {
                UserId = graphic.UserId,
                GraphicDescription = graphic.GraphicDescription,
                GraphicHref = graphic.GraphicHref,
                Price = graphic.Price,
                CreationDate = graphic.CreationDate,
                LikesCount = graphic.LikesCount,
                CommentsCount = graphic.CommentsCount,
                IsActivePost = graphic.IsActivePost,
                IsBannedPost = graphic.IsBannedPost
            };
            return graphicViewModel;
        }
    }
}