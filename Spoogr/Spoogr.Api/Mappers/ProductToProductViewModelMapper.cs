﻿using Spoogr.Api.ViewModels;
using Spoogr.Data.Sql.DAO;

namespace Spoogr.Api.Mappers
{
    public class ProductToProductViewModelMapper
    {
        public static ProductViewModel ProductToProductViewModel(Domain.Product.Product product)
        {
            var productViewModel = new ProductViewModel
            {
                ProductId = product.ProductId,
                UserId = product.UserId,
                CommentsCount = product.CommentsCount,
                CreationDate = product.CreationDate,
                IsActiveProduct = product.IsActiveProduct,
                IsBannedProduct = product.IsBannedProduct,
                Price = product.Price,
                ProductDescription = product.ProductDescription,
                ProductHref = product.ProductHref,
                ProductName = product.ProductName,
            };
            return productViewModel;
        }
    }
}