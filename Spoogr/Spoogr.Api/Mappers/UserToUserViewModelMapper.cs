﻿using Spoogr.Api.ViewModels;

namespace Spoogr.Api.Mappers
{
    public class UserToUserViewModelMapper
    {
        public static UserViewModel UserToUserViewModel(Domain.User.User user)
        {
            var userViewModel = new UserViewModel
            {
                UserId = user.UserId,
                Email = user.Email,
                Gender = user.Gender,
                UserName = user.UserName,
                BirthDate = user.BirthDate,
                EditionDate = user.EditionDate,
                RegistrationDate = user.RegistrationDate,
                IsActiveUser = user.IsActiveUser,
                IsBannedUser = user.IsBannedUser,
                IconHref = user.IconHref,
                GraphicsCount = user.GraphicsCount,
                FollowersCount = user.FollowersCount,
                FollowingCount = user.FollowingCount,
                AccountDescription = user.AccountDescription,
            };
            return userViewModel;
        }
    }
}