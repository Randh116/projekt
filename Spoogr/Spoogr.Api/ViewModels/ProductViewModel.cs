﻿using System;
using Spoogr.Common.Enums;

namespace Spoogr.Api.ViewModels
{
    public class ProductViewModel
    {
        public int ProductId { get; set; }
        public int UserId { get; set; }
        public bool IsActiveProduct { get; set; }
        public bool IsBannedProduct { get; set; }
        public DateTime CreationDate { get; set; }
        public int CommentsCount { get; set; }
        public int Price { get; set; }
        public string ProductDescription { get; set; }
        public string ProductHref { get; set; }
        public string ProductName { get; set; }
    }
}