﻿using System;
using Spoogr.Common.Enums;

namespace Spoogr.Api.ViewModels
{
    public class UserViewModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime EditionDate { get; set; }
        public Gender Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public bool IsBannedUser { get; set; }
        public bool IsActiveUser { get; set; }
        public int GraphicsCount { get; set; }
        public int FollowersCount { get; set; }
        public int FollowingCount { get; set; }
        public string AccountDescription { get; set; }
        public string IconHref { get; set; }
    }
}