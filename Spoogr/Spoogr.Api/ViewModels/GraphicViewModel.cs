﻿using System;
using Spoogr.Common.Enums;

namespace Spoogr.Api.ViewModels
{
    public class GraphicViewModel
    {
        public int GraphicId { get; set; }
        public int UserId { get; set; }
        public string GraphicDescription { get; set; }
        public string GraphicHref { get; set; }
        public int Price { get; set; }
        public DateTime CreationDate { get; set; }
        public int LikesCount { get; set; }
        public int CommentsCount { get; set; }
        public bool IsBannedPost { get; set; }
        public bool IsActivePost { get; set; } 
    }
}