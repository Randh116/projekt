﻿using FluentValidation;
using EditProduct = Spoogr.IServices.Requests.EditProduct;

namespace Spoogr.Api.Validation
{
    public class EditProductValidator: AbstractValidator<EditProduct>
    {
        public EditProductValidator()
        {
            RuleFor(x => x.ProductName).NotNull();
            RuleFor(x => x.Price).NotNull();
        }
    }
}