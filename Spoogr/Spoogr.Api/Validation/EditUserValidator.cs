﻿using FluentValidation;
using Spoogr.Api.BindingModels;
using Spoogr.IServices.Requests;
using EditUser = Spoogr.IServices.Requests.EditUser;

namespace Spoogr.Api.Validation
{
    public class EditUserValidator : AbstractValidator<EditUser>
    {
        public EditUserValidator()
        {
            RuleFor(x => x.UserName).NotNull();
            RuleFor(x => x.BirthDate).NotNull();
            RuleFor(x => x.Email).NotNull();
            RuleFor(x => x.Gender).NotNull();
        }
    }
}
