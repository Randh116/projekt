﻿using System.Data;
using FluentValidation;
using Spoogr.Api.BindingModels;
using Spoogr.IServices.Requests;
using CreateUser = Spoogr.IServices.Requests.CreateUser;

namespace Spoogr.Api.Validation
{
    public class CreateUserValidator: AbstractValidator<CreateUser>
    {
        public CreateUserValidator()
        {
            RuleFor(x => x.UserName).NotNull();
            RuleFor(x => x.BirthDate).NotNull();
            RuleFor(x => x.Email).NotNull();
            RuleFor(x => x.Gender).NotNull();
        }
    }
}