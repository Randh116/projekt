﻿using System.Data;
using FluentValidation;
using CreateProduct = Spoogr.IServices.Requests.CreateProduct;

namespace Spoogr.Api.Validation
{
    public class CreateProductValidator: AbstractValidator<CreateProduct>
    {
        public CreateProductValidator()
        {
            RuleFor(x => x.UserId).NotNull();
            RuleFor(x => x.ProductName).NotNull();
            RuleFor(x => x.Price).NotNull();
        }
    }
}