﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Spoogr.IData.PreparedProduct;

namespace Spoogr.Data.Sql.PreparedProduct
{
    public class PreparedProductRepository: IPreparedProductRepository
    {
        private readonly SpoogrDbContext _context;

        public PreparedProductRepository(SpoogrDbContext context)
        {
            _context = context;
        }

        public async Task<int> AddPreparedProduct(Domain.PreparedProduct.PreparedProduct preparedProduct)
        {
            var preparedProductDAO = new DAO.PreparedProduct
            {
                UserId = preparedProduct.UserId,
                GraphicId = preparedProduct.GraphicId,
                ProductId = preparedProduct.ProductId,
                OrderId = preparedProduct.OrderId,
                Price = preparedProduct.Price
            };
            await _context.AddAsync(preparedProductDAO);
            await _context.SaveChangesAsync();
            return preparedProductDAO.PreparedProductId;
        }

        public async Task<Domain.PreparedProduct.PreparedProduct> GetPreparedProduct(int preparedProductId)
        {
            var preparedProduct =
                await _context.PreparedProduct.FirstOrDefaultAsync(x => x.PreparedProductId == preparedProductId);
            return new Domain.PreparedProduct.PreparedProduct(preparedProduct.PreparedProductId,
                preparedProduct.UserId,
                preparedProduct.GraphicId,
                preparedProduct.ProductId,
                preparedProduct.OrderId,
                preparedProduct.Price
            );
        }

        // nie ma edita
        
        public async Task DeletePreparedProduct(int preparedProductId)
        {
            var preparedProduct =
                await _context.PreparedProduct.FirstOrDefaultAsync(x => x.PreparedProductId == preparedProductId);

            _context.PreparedProduct.Remove(preparedProduct);

            await _context.SaveChangesAsync();
        }
    }
}