﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore.Internal;
using Org.BouncyCastle.Crypto.Signers;
using Spoogr.Common.Enums;
using Spoogr.Common.Extensions;
using Spoogr.Data.Sql.DAO;
using Spoogr.Data.Sql;

namespace Spoogr.Data.Sql.Migrations
{
    public class DatabaseSeed
    {
        private readonly SpoogrDbContext _context;
        
        public DatabaseSeed(SpoogrDbContext context)
        {
            _context = context;
        }
        
        public void Seed()
        {
  
            #region CreateUsers
            var userList = BuildUserList();
            _context.User.AddRange(userList);
            _context.SaveChanges();
            #endregion
            
            #region CreateUserRelations
            var userRelationList = BuildUserRelationList(userList);
            _context.UserRelation.AddRange(userRelationList);
            _context.SaveChanges();
            #endregion
            
            #region CreateCategories
            var categoryList = BuildCategoryList();
            _context.Category.AddRange(categoryList);
            _context.SaveChanges();
            #endregion
            
            #region CreateGraphic
            var graphicList = BuildGraphicList(userList);
            _context.Graphic.AddRange(graphicList);
            _context.SaveChanges();
            #endregion
            
            #region CreateProduct
            var productList = BuildProductList(userList);
            _context.Product.AddRange(productList);
            _context.SaveChanges();
            #endregion
            
            #region CreateProductCategory
            var productCategoryList = BuildProductCategoryList(productList, categoryList);
            _context.ProductCategory.AddRange(productCategoryList);
            _context.SaveChanges();
            #endregion
            
            #region CreateOrder
            var orderList = BuildOrderList(userList);
            _context.Order.AddRange(orderList);
            _context.SaveChanges();
            #endregion
            
            #region CreatePreference
            var preferenceList = BuildPreferenceList(graphicList, userList);
            _context.Preference.AddRange(preferenceList);
            _context.SaveChanges();
            #endregion
            
            #region CreateComment
            var commentList = BuildCommentList(graphicList, userList);
            _context.Comment.AddRange(commentList);
            _context.SaveChanges();
            #endregion
            
            #region CreatePreparedProduct
            var preparedProductList = BuildPreparedProductList(graphicList, productList, userList, orderList);
            _context.PreparedProduct.AddRange(preparedProductList);
            _context.SaveChanges();
            #endregion
            

        }
        
        // OK
        private IEnumerable<DAO.User> BuildUserList()
        {
            var userList = new List<DAO.User>();
            
            var user = new DAO.User()
            {
                UserName = "Rafal",
                Email = "rwawrzynek6@gmail.com",
                RegistrationDate = DateTime.Now.AddYears(-3),
                EditionDate = DateTime.Now.AddYears(-3),
                BirthDate = new DateTime(1997,8 , 06),
                Gender = Gender.Male,
                IsActiveUser = true,
                IsBannedUser = false,
                FollowersCount = 0,
                FollowingCount = 0,
                GraphicsCount = 0,
                AccountDescription = "super desc",
                IconHref = "https://www.bhphotovideo.com/images/images500x500/LEE_Filters_115R_Peacock_Blue_Color_Effect_688218.jpg"
            };
            userList.Add(user);

            var user2 = new DAO.User()
            {
                UserName = "Parowa",
                Email = "parowa@student.po.edu.pl",
                RegistrationDate = DateTime.Now.AddYears(-2),
                EditionDate = DateTime.Now.AddYears(-2),
                BirthDate = new DateTime(1994, 6, 7),
                Gender = Gender.Male,
                IsActiveUser = true,
                IsBannedUser = false,
                FollowersCount = 0,
                FollowingCount = 0,
                GraphicsCount = 0,
                AccountDescription = "jestem parowom",
                IconHref = "https://www.bhphotovideo.com/images/images500x500/LEE_Filters_115R_Peacock_Blue_Color_Effect_688218.jpg",
            };
            userList.Add(user2);
            
            for (int i = 3; i <= 20; i++)
            {
                var user3 = new DAO.User
                {
                    UserName = "user" + i,
                    Email = "user" + i + "@student.po.edu.pl",
                    RegistrationDate = DateTime.Now.AddYears(-2),
                    EditionDate = DateTime.Now.AddYears(-2),
                    BirthDate = new DateTime(1994, 6, 7),
                    Gender = i % 2 == 0 ? Gender.Female : Gender.Male,
                    IsActiveUser = true,
                    IsBannedUser = false,
                    FollowersCount = 0,
                    FollowingCount = 0,
                    GraphicsCount = 0,
                    AccountDescription = "super desc",
                    IconHref = "https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Solid_black.svg/2000px-Solid_black.svg.png"
                };
                userList.Add(user3);
            }

            return userList;
        }
        
        // OK
        private IEnumerable<DAO.UserRelation> BuildUserRelationList(
            IEnumerable<DAO.User> userList)
        {
            var userRelations = new List<DAO.UserRelation>();
            foreach (var user in userList)
            {
                if(user.UserName!="Rafal")
                    userRelations.Add(new DAO.UserRelation
                    {
                        RelatedUserId = userList.First(x=>x.UserName=="Rafal").UserId,
                        RelatingUserId = user.UserId,
                        FollowDate = DateTime.Now,
                        Follow = true
                    });
                if(user.UserName!="Rafal"&&user.UserName=="Parowa")
                    userRelations.Add(new DAO.UserRelation
                    {
                        RelatedUserId = userList.First(x=>x.UserName=="Rafal").UserId,
                        RelatingUserId = user.UserId,
                        FollowDate = DateTime.Now,
                        Follow = true
                    });
            }
            foreach (var user in userList)
            {
                if(user.UserName!="Rafal"&&user.UserName!="Parowa")
                    userRelations.Add(new DAO.UserRelation
                    {
                        RelatedUserId = userList.First(x=>x.UserName=="Rafal").UserId,
                        RelatingUserId = user.UserId,
                        FollowDate = DateTime.Now,
                        Follow = true
                    });
                if(user.UserName!="Parowa"&&user.UserName=="Rafal")
                    userRelations.Add(new DAO.UserRelation
                    {
                        RelatedUserId = userList.First(x=>x.UserName=="Parowa").UserId,
                        RelatingUserId = user.UserId,
                        BlockDate = DateTime.Now,
                        Block = true
                    });
            }
            
            return userRelations;
        }
        
        // OK
        private IEnumerable<DAO.Category> BuildCategoryList()
        {
            var categoryList = new List<DAO.Category>
            {
                new DAO.Category
                {
                    CategoryName = "Hoodies",
                    ImageHref =
                        "https://www.tablespoon.com/-/media/Images/recipe-hero/appetizer/mini-spinach-lasagna-roll-ups_hero.jpg"
                },
                new DAO.Category
                {
                    CategoryName = "T-shirts",
                    ImageHref = "https://i.pinimg.com/originals/74/71/e5/7471e50e0d081bd2150dfe399f951928.jpg"

                },
                new DAO.Category
                {
                    CategoryName = "Trousers",
                    ImageHref =
                        "https://assets.marthastewart.com/styles/wmax-1500/d38/msl-kitchen-sponge-cakes-0554-md110059/msl-kitchen-sponge-cakes-0554-md110059_horiz.jpg?itok=Bu4cGnhF"
                },
                new DAO.Category
                {
                    CategoryName = "Hats",
                    ImageHref =
                        "https://www.themagicalslowcooker.com/wp-content/uploads/2017/08/easy-spaghetti-dinner-5-of-5.jpg"
                },
                new DAO.Category
                {
                    CategoryName = "Shorts",
                    ImageHref = "https://nutritionfacts.org/app/uploads/2017/03/Smoothies.jpeg"
                },
                new DAO.Category
                {
                    CategoryName = "Jackets",
                    ImageHref =
                        "https://static.independent.co.uk/s3fs-public/styles/article_small/public/thumbnails/image/2018/01/12/12/healthy-avo-food.jpg"
                },
            };
            return categoryList;
        }
        
        // OK
        private IEnumerable<DAO.Graphic> BuildGraphicList(IEnumerable<DAO.User> userList)
        {
            var descs = new List<string>
            {
                "Make me Anon lvl 28.",
                "I can remember my old man was a fanatic leczo.",
                "Ever since I can remember just cooked.",
                "It started innocently enough.",
                "somehow under communism and went with his mother and older brother to Hungary",
                "So his posmakowało that the entire pot to the local Makłowiczowi",
                "and has eaten with his mother and brother for toddler and began to off.",
                "To this day her mother tells how the banana on the snout has eaten ladle by ladle",
                "until diarrhea got. hmm communism as, the station does not, and what piece would stop",
                "in the bushes and orange. But has eaten with abandon until it was over.",
                "hmm the pot and says, Grazyna, tomorrow we leczo for dinner!",
                "Old panic in the eyes, the brother began to cry.",
                "After returning home he going to market in search of peppers.",
                "hmm nakupował with 30 kilos, he dragged the boiler to Preserving and interceded on gazówkę.",
                "What I was upset because I happened to overhaul the bloc had!",
                "He ran to the fitters and began their fight ladle, until his militia into 24 closed.",
                "Now whole tells all and sundry how he was not sitting under communism for freedom of the country.",
                "He sat for leczo. now able to stand on garem four hours and sent me two times daily peppers to the store",
                "because he too banned. The saleswoman saw me the out of the store without a word bag with peppers.",
                "Tache is a on the third floor with no elevator.",
            };

            var hrefs = new List<string>
            {
                "https://vignette.wikia.nocookie.net/spookys-house-of-jump-scares/images/f/fc/Spooky.spr/revision/latest/top-crop/width/360/height/450?cb=20190125195130&path-prefix=pl",
                "https://i.pinimg.com/originals/7a/fb/65/7afb65bc91fb0436bf98aa5ce79febb9.jpg",
                "https://blogs.plos.org/wp-content/uploads/2017/10/maxresdefault-690x320.jpg",
                "https://www.boulderweekly.com/wp-content/uploads/2018/10/WEED-CANVAS-768x481.jpg",
                "https://cdn.drawception.com/images/panels/2014/8-30/E3AraS8xzX-8.png",
                "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSpoE4rUgN3FHTsaO1ZZ9egWqEfIVqG6AdGlUr4qfFwUgwfG4DU&usqp=CAU",
                "https://i.pinimg.com/originals/fd/ef/8f/fdef8f68038f64ea38a0ac0ea3d342f0.jpg",
            };
            
            var graphicsList = new List<DAO.Graphic>();
            
            for (int i = 0; i < 49; i++)
            {
                descs.Shuffle();
                hrefs.Shuffle();
                graphicsList.Add(new DAO.Graphic
                {
                    GraphicDescription = i+1+descs.First(),
                    GraphicHref = hrefs.First(),
                    CreationDate = DateTime.Now,
                    EditionDate = DateTime.Now,
                    CommentsCount = 0,
                    IsActivePost = true,
                    IsBannedPost = false,
                    LikesCount = 0,
                    Price = 0,
                });
            }

            var rand = new Random();
            var userCount = userList.ToList().Count;
            foreach (var graphic in graphicsList)
            {
                graphic.UserId = userList.ToList()[rand.Next(userCount)].UserId;
            }
            
            return graphicsList;
        }
        
        // OK
        private IEnumerable<DAO.Product> BuildProductList(IEnumerable<DAO.User> userList)
        {
            var descs = new List<string>
            {
                "Make me Anon lvl 28.",
                "I can remember my old man was a fanatic leczo.",
                "Ever since I can remember just cooked.",
                "It started innocently enough.",
                "somehow under communism and went with his mother and older brother to Hungary",
                "So his posmakowało that the entire pot to the local Makłowiczowi",
                "and has eaten with his mother and brother for toddler and began to off.",
                "To this day her mother tells how the banana on the snout has eaten ladle by ladle",
                "until diarrhea got. hmm communism as, the station does not, and what piece would stop",
                "in the bushes and orange. But has eaten with abandon until it was over.",
                "hmm the pot and says, Grazyna, tomorrow we leczo for dinner!",
                "Old panic in the eyes, the brother began to cry.",
                "After returning home he going to market in search of peppers.",
                "hmm nakupował with 30 kilos, he dragged the boiler to Preserving and interceded on gazówkę.",
                "What I was upset because I happened to overhaul the bloc had!",
                "He ran to the fitters and began their fight ladle, until his militia into 24 closed.",
                "Now whole tells all and sundry how he was not sitting under communism for freedom of the country.",
                "He sat for leczo. now able to stand on garem four hours and sent me two times daily peppers to the store",
                "because he too banned. The saleswoman saw me the out of the store without a word bag with peppers.",
                "Tache is a on the third floor with no elevator.",
            };

            var hrefs = new List<string>
            {
                "https://vignette.wikia.nocookie.net/spookys-house-of-jump-scares/images/f/fc/Spooky.spr/revision/latest/top-crop/width/360/height/450?cb=20190125195130&path-prefix=pl",
                "https://i.pinimg.com/originals/7a/fb/65/7afb65bc91fb0436bf98aa5ce79febb9.jpg",
                "https://blogs.plos.org/wp-content/uploads/2017/10/maxresdefault-690x320.jpg",
                "https://www.boulderweekly.com/wp-content/uploads/2018/10/WEED-CANVAS-768x481.jpg",
                "https://cdn.drawception.com/images/panels/2014/8-30/E3AraS8xzX-8.png",
                "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSpoE4rUgN3FHTsaO1ZZ9egWqEfIVqG6AdGlUr4qfFwUgwfG4DU&usqp=CAU",
                "https://i.pinimg.com/originals/fd/ef/8f/fdef8f68038f64ea38a0ac0ea3d342f0.jpg",
            };
            
            var productsList = new List<DAO.Product>();
            
            for (int i = 0; i < 49; i++)
            {
                descs.Shuffle();
                hrefs.Shuffle();
                productsList.Add(new DAO.Product
                {
                    ProductName = "product"+i,
                    ProductDescription = i+1+descs.First(),
                    ProductHref = hrefs.First(),
                    CreationDate = DateTime.Now,
                    EditionDate = DateTime.Now,
                    CommentsCount = 0,
                    IsActiveProduct = true,
                    IsBannedProduct = false,
                    Price = 0,
                });
            }

            var rand = new Random();
            var userCount = userList.ToList().Count;
            foreach (var product in productsList)
            {
                product.UserId = userList.ToList()[rand.Next(userCount)].UserId;
            }
            
            return productsList;
        }
        
        // OK
        private IEnumerable<DAO.ProductCategory> BuildProductCategoryList(
            IEnumerable<DAO.Product> productsList, 
            IEnumerable<DAO.Category> categoryList)
        {
            var productCategoryList = new List<DAO.ProductCategory>();
            productsList.ToList().Shuffle();
            categoryList.ToList().Shuffle();
            var rnd = new Random();
            for (int i = 0; i < productsList.Count(); i++)
            {
                var index = rnd.Next(0, categoryList.Count());

                productCategoryList.Add(new DAO.ProductCategory
                {
                    CategoryId = categoryList.ToList()[index].CategoryId,
                    ProductId = productsList.ToList()[i].ProductId
                });
            }
            return productCategoryList;
        }

        // OK
        private IEnumerable<DAO.Order> BuildOrderList(
            IEnumerable<DAO.User> userList)
        {
            var orderList = new List<DAO.Order>();
            for (int i = 0; i < 30; i++)
            {
                orderList.Add(new DAO.Order
                {
                    OrderDate = DateTime.Now.AddMonths(-10).AddDays(i),
                    PaymentDate = DateTime.Now.AddMonths(-10).AddDays(1+i),
                    OrderItemsCount = 0,
                    Price = 0,
                });
            }
            
            var rnd = new Random();
            var userCount = userList.ToList().Count;
            foreach (var order in orderList)
            {
                order.UserId = userList.ToList()[rnd.Next(userCount)].UserId;
            }

            return orderList;
        }
        
        // OK
        private IEnumerable<DAO.Preference> BuildPreferenceList(
            IEnumerable<DAO.Graphic> graphicList,
            IEnumerable<DAO.User> userList)
        {
            var preferenceList = new List<DAO.Preference>();
            foreach (var user in userList)
            {
                if (user.UserName != "Rafal")
                {
                    graphicList.ToList().Shuffle();
                    preferenceList.Add(new DAO.Preference
                    {
                        GraphicId = graphicList.ToList()[0].GraphicId,
                        UserId = user.UserId,
                        LikeDate = new DateTime(),
                        Like = true,
                    });
                    graphicList.ToList()[0].LikesCount += 1;
                }
            }

            return preferenceList;
        }

        // OK
        private IEnumerable<DAO.Comment> BuildCommentList(
            IEnumerable<DAO.Graphic> graphicList,
            IEnumerable<DAO.User> userList)
        {
            var commentList = new List<DAO.Comment>();
            var counter = 0;
            var rand = new Random();
            var postsCount = graphicList.ToList().Count;
            foreach (var user in userList)
            {
                counter++;
                if (user.UserName != "Rafal")
                {
                    var graphicId = rand.Next(postsCount);
                    commentList.Add(new DAO.Comment
                    {
                        GraphicId = graphicList.ToList()[graphicId].GraphicId,
                        UserId = user.UserId,
                        CommentBody = "comment"+counter,
                        CommentDate = DateTime.Now,
                        IsActiveComment = true,
                        IsBannedComment = false
                    });
                    graphicList.ToList()[graphicId].CommentsCount += 1;
                }
            }
            return commentList;
        }

        private IEnumerable<DAO.PreparedProduct> BuildPreparedProductList(
            IEnumerable<DAO.Graphic> graphicList,
            IEnumerable<DAO.Product> productList,
            IEnumerable<DAO.User> userList,
            IEnumerable<DAO.Order> orderList)
        {
            var preparedProductList = new List<DAO.PreparedProduct>();
            
            graphicList.ToList().Shuffle();
            productList.ToList().Shuffle();
            userList.ToList().Shuffle();
            orderList.ToList().Shuffle();
            
            var rand = new Random();
            for (int i = 0, j = 0; i < graphicList.Count(); i++)
            {
                var index = rand.Next(1);
                
                if (i == 25) j = 0;
                
                preparedProductList.Add(new DAO.PreparedProduct
                {
                    UserId = userList.ToList()[index].UserId,
                    GraphicId = graphicList.ToList()[i].GraphicId,
                    ProductId = productList.ToList()[i].ProductId,
                    OrderId = orderList.ToList()[index].OrderId,
                    Price = rand.Next(0,100)
                });
                j++;
            }

            return preparedProductList;
        }

    }
    
}