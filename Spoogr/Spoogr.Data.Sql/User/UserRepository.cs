﻿using System.Linq;
using System.Threading.Tasks;
using Spoogr.IData.User;
using Google.Protobuf.WellKnownTypes;
using Microsoft.EntityFrameworkCore;
using Spoogr.Data.Sql.DAO;

namespace Spoogr.Data.Sql.User
{
    public class UserRepository : IUserRepository
    {
        private readonly SpoogrDbContext _context;

        public UserRepository(SpoogrDbContext context)
        {
            _context = context;
        }

        public async Task<int> AddUser(Domain.User.User user)
        {
            var userDAO = new DAO.User
            {
                Email = user.Email,
                UserName = user.UserName, 
                Gender = user.Gender,
                BirthDate = user.BirthDate,
                RegistrationDate = user.RegistrationDate,
                EditionDate = user.EditionDate,
                IsActiveUser = user.IsActiveUser,
                IsBannedUser = user.IsBannedUser,
                GraphicsCount = user.GraphicsCount,
                FollowersCount = user.FollowersCount,
                FollowingCount = user.FollowingCount,
            };
            await _context.AddAsync(userDAO);
            await _context.SaveChangesAsync();
            return userDAO.UserId;
        }

        public async Task<Domain.User.User> GetUser(int userId)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.UserId == userId);
            return new Domain.User.User(user.UserId,
                user.UserName,
                user.Email,
                user.RegistrationDate,
                user.EditionDate,
                user.BirthDate,
                user.Gender,
                user.IsBannedUser,
                user.IsActiveUser,
                user.GraphicsCount,
                user.FollowersCount,
                user.FollowingCount,
                user.AccountDescription,
                user.IconHref);
        }
        
        public async Task<Domain.User.User> GetUser(string userName)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.UserName == userName);
            return new Domain.User.User(user.UserId,
                user.UserName,
                user.Email,
                user.RegistrationDate,
                user.EditionDate,
                user.BirthDate,
                user.Gender,
                user.IsBannedUser,
                user.IsActiveUser,
                user.GraphicsCount,
                user.FollowersCount,
                user.FollowingCount,
                user.AccountDescription,
                user.IconHref);
        }

        public async Task EditUser(Domain.User.User user)
        {
            var editUser = await _context.User.FirstOrDefaultAsync(x => x.UserId == user.UserId);
            editUser.UserName = user.UserName;
            editUser.Email = user.Email;
            editUser.Gender = user.Gender;
            editUser.BirthDate = user.BirthDate;
            editUser.EditionDate = user.EditionDate;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteUser(int userId)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.UserId == userId);

            _context.User.Remove(user);

            await _context.SaveChangesAsync();
        }
    }
}