﻿using Spoogr.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Spoogr.Data.Sql.DAOConfigurations
{
    public class OrderItemConfiguration: IEntityTypeConfiguration<OrderItem>
    {
        public void Configure(EntityTypeBuilder<OrderItem> builder)
        {
            builder.Property(c => c.Quantity).IsRequired();

            builder.HasOne(x => x.Order)
                .WithMany(x => x.OrderItems)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.OrderId);
            
            // relacja 1 do 1, jeden order item nie moze posiadac kilku preparedproductow
            // orderitem ma byc unikalny dla kazdego zamowienia o okreslonej quantity
            // 
            // dobra jednak zartowalem inaczej mi nie chce dzialac kiedys to poprawie
            builder.HasOne(x => x.PreparedProduct)
                .WithMany(x => x.OrderItems)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.PreparedProductId);

            builder.ToTable("OrderItem");
        }
    }
}