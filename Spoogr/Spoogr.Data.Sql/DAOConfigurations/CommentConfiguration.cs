﻿using Spoogr.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Spoogr.Data.Sql.DAOConfigurations
{
    public class CommentConfiguration: IEntityTypeConfiguration<DAO.Comment>
    {
        public void Configure(EntityTypeBuilder<DAO.Comment> builder)
        {
            builder.Property(c => c.CommentBody).IsRequired();
            //ustawienie propercji typu Boolean jako kolumna typu tinyint
            
            //builder.Property(c => c.IsActiveComment).HasColumnType("tinyint(1)");
            //builder.Property(c => c.IsBannedComment).HasColumnType("tinyint(1)");
            
            //konfiguracja zależności jeden do wielu 
            builder.HasOne(x => x.Graphic)
                .WithMany(x => x.Comments)
                // Konfiguruje sposób, w jaki operacja usuwania jest stosowana
                // do jednostek zależnych w relacji, gdy jednostka główna zostanie
                // usunięta lub relacja zostanie zerwana.
                .OnDelete(DeleteBehavior.Restrict)
                //ustawienie klucza głównego
                .HasForeignKey(x => x.GraphicId);
            
            builder.HasOne(x => x.User)
                .WithMany(x => x.Comments)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.UserId);
            
            builder.HasOne(x => x.ParentComment)
                .WithMany(x => x.SubComments)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.ParentCommentId);
            
            builder.ToTable("Comment");
        }
    }

}