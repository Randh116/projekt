﻿using Spoogr.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Spoogr.Data.Sql.DAO;

namespace Spoogr.Data.Sql.DAOConfigurations
{
    public class ProductConfiguration: IEntityTypeConfiguration<DAO.Product>
    {
        public void Configure(EntityTypeBuilder<DAO.Product> builder)
        {
            builder.Property(c => c.ProductDescription).IsRequired();
            //builder.Property(c => c.IsBannedProduct).HasColumnType("tinyint(1)");
            //builder.Property(c => c.IsActiveProduct).HasColumnType("tinyint(1)");

            builder.HasOne(x => x.User)
                .WithMany(x => x.Products)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.UserId);

            builder.ToTable("Product");
        }
    }
}