﻿using Spoogr.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Spoogr.Data.Sql.DAOConfigurations
{
    public class PreferenceConfiguration: IEntityTypeConfiguration<DAO.Preference>
    {
        public void Configure(EntityTypeBuilder<DAO.Preference> builder)
        {
            //builder.Property(c => c.Like).HasColumnType("tinyint(1)");
            
            builder.HasOne(x => x.Graphic)
                .WithMany(x => x.Preferences)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.GraphicId);

            builder.HasOne(x => x.User)
                .WithMany(x => x.Preferences)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.UserId);
            
            builder.ToTable("Preference");
        }
    }
}