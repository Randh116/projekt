﻿using Spoogr.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Spoogr.Data.Sql.DAOConfigurations
{
    public class PreparedProductConfiguration: IEntityTypeConfiguration<DAO.PreparedProduct>
    {
        public void Configure(EntityTypeBuilder<DAO.PreparedProduct> builder)
        {
            builder.Property(c => c.Price).IsRequired();

            builder.HasOne(x => x.Graphic)
                .WithMany(x => x.PreparedProducts)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.GraphicId);

            builder.HasOne(x => x.Product)
                .WithMany(x => x.PreparedProducts)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.ProductId);

            builder.HasOne(x => x.User)
                .WithMany(x => x.PreparedProducts)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.UserId);

            builder.HasOne(x => x.Order)
                .WithMany(x => x.PreparedProducts)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.OrderId);

            builder.ToTable("PreparedProduct");
        }
    }
}