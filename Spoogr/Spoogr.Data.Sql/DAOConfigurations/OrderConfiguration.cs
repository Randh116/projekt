﻿using System.Security.Cryptography.X509Certificates;
using Spoogr.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Spoogr.Data.Sql.DAOConfigurations
{
    public class OrderConfiguration: IEntityTypeConfiguration<DAO.Order>
    {
        public void Configure(EntityTypeBuilder<DAO.Order> builder)
        {
            builder.Property(c => c.OrderDate).IsRequired();
            builder.Property(c => c.Price).IsRequired();

            builder.HasOne(x => x.User)
                .WithMany(x => x.Orders)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.UserId);

            builder.ToTable("Order");
        }
    }
}