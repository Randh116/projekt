﻿using Spoogr.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Spoogr.Data.Sql.DAOConfigurations
{
    public class ProductCategoryConfiguration: IEntityTypeConfiguration<DAO.ProductCategory>
    {
        public void Configure(EntityTypeBuilder<DAO.ProductCategory> builder)
        {
            builder.HasOne(x => x.Product)
                .WithMany(x => x.ProductCategories)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.ProductId);

            builder.HasOne(x => x.Category)
                .WithMany(x => x.ProductCategories)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.CategoryId);

            builder.ToTable("ProductCategory");
        }
    }
}