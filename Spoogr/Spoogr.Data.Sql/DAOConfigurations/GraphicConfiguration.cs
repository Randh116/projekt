﻿using Spoogr.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Spoogr.Data.Sql.DAOConfigurations
{
    public class GraphicConfiguration : IEntityTypeConfiguration<DAO.Graphic>
    {
        public void Configure(EntityTypeBuilder<DAO.Graphic> builder)
        {
            builder.Property(c => c.GraphicDescription).IsRequired();
            builder.Property(c => c.IsActivePost).IsRequired();
            builder.Property(c => c.IsBannedPost).IsRequired();
            
            builder.HasOne(x => x.User)
                .WithMany(x => x.Graphics)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.UserId);
            
            builder.ToTable("Graphic");
        }
    }
}