﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic;
using Spoogr.IData.Preference;

namespace Spoogr.Data.Sql.Preference
{
    public class PreferenceRepository: IPreferenceRepository
    {
        private readonly SpoogrDbContext _context;

        public PreferenceRepository(SpoogrDbContext context)
        {
            _context = context;
        }

        public async Task<int> AddPreference(Domain.Preference.Preference preference)
        {
            var preferenceDAO = new DAO.Preference
            {
                UserId = preference.UserId,
                GraphicId = preference.GraphicId,
                Like = preference.Like,
                LikeDate = preference.LikeDate
            };
            await _context.AddAsync(preferenceDAO);
            await _context.SaveChangesAsync();
            return preferenceDAO.PreferenceId;
        }

        public async Task<Domain.Preference.Preference> GetPreference(int preferenceId)
        {
            var preference = await _context.Preference.FirstOrDefaultAsync(x => x.PreferenceId == preferenceId);
            return new Domain.Preference.Preference(preference.PreferenceId,
                preference.UserId,
                preference.GraphicId,
                preference.Like,
                preference.LikeDate
            );
        }

        public async Task EditPreference(Domain.Preference.Preference preference)
        {
            var editPreference =
                await _context.Preference.FirstOrDefaultAsync(x => x.PreferenceId == preference.PreferenceId);
            editPreference.UserId = preference.UserId;
            editPreference.GraphicId = preference.GraphicId;
            editPreference.Like = preference.Like;
            editPreference.LikeDate = preference.LikeDate;
        }

        public async Task DeletePreference(int preferenceId)
        {
            var preference = await _context.Preference.FirstOrDefaultAsync(x => x.PreferenceId == preferenceId);

            _context.Preference.Remove(preference);

            await _context.SaveChangesAsync();
        }
    }
}