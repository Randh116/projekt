﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Spoogr.IData.ProductCategory;

namespace Spoogr.Data.Sql.ProductCategory
{
    public class ProductCategoryRepository: IProductCategoryRepository
    {
        private readonly SpoogrDbContext _context;

        public ProductCategoryRepository(SpoogrDbContext context)
        {
            _context = context;
        }

        public async Task<int> AddProductCategory(Domain.ProductCategory.ProductCategory productCategory)
        {
            var productCategoryDAO = new DAO.ProductCategory
            {
                ProductId = productCategory.ProductId,
                CategoryId = productCategory.CategoryId
            };
            await _context.AddAsync(productCategoryDAO);
            await _context.SaveChangesAsync();
            return productCategoryDAO.ProductCategoryId;
        }

        public async Task<Domain.ProductCategory.ProductCategory> GetProductCategory(int productCategoryId)
        {
            var productCategory =
                await _context.ProductCategory.FirstOrDefaultAsync(x => x.ProductCategoryId == productCategoryId);
            return new Domain.ProductCategory.ProductCategory(productCategory.ProductCategoryId,
                productCategory.ProductId,
                productCategory.CategoryId
            );
        }

        // nie ma edita
        
        public async Task DeleteProductCategory(int productCategoryId)
        {
            var productCategory =
                await _context.ProductCategory.FirstOrDefaultAsync(x => x.ProductCategoryId == productCategoryId);

            _context.ProductCategory.Remove(productCategory);

            await _context.SaveChangesAsync();
        }
    }
}