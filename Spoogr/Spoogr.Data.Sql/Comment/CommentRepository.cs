﻿using System.Linq;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Microsoft.EntityFrameworkCore;
using Spoogr.Data.Sql.DAO;
using Spoogr.IData.Comment;

namespace Spoogr.Data.Sql.Comment
{
    public class CommentRepository: ICommentRepository
    {
        private readonly SpoogrDbContext _context;

        public CommentRepository(SpoogrDbContext context)
        {
            _context = context;
        }

        public async Task<int> AddComment(Domain.Comment.Comment comment)
        {
            var commentDAO = new DAO.Comment
            {
                ParentCommentId = comment.ParentCommentId,
                GraphicId = comment.GraphicId,
                UserId = comment.UserId,
                CommentBody = comment.CommentBody,
                CommentDate = comment.CommentDate,
                IsActiveComment = comment.IsActiveComment,
                IsBannedComment = comment.IsBannedComment
            };
            await _context.AddAsync(commentDAO);
            await _context.SaveChangesAsync();
            return commentDAO.CommentId;
        }

        public async Task<Domain.Comment.Comment> GetComment(int commentId)
        {
            var comment = await _context.Comment.FirstOrDefaultAsync(x => x.CommentId == commentId);
            return new Domain.Comment.Comment(comment.CommentId,
                comment.ParentCommentId,
                comment.GraphicId,
                comment.UserId,
                comment.CommentBody,
                comment.CommentDate,
                comment.IsActiveComment,
                comment.IsBannedComment
            );
        }

        public async Task EditComment(Domain.Comment.Comment comment)
        {
            var editComment = await _context.Comment.FirstOrDefaultAsync(x => x.CommentId == comment.CommentId);
            editComment.CommentBody = comment.CommentBody;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteComment(int commentId)
        {
            var comment = await _context.Comment.FirstOrDefaultAsync(x => x.CommentId == commentId);

            _context.Comment.Remove(comment);

            await _context.SaveChangesAsync();
        }
    }
}