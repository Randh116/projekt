﻿using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Spoogr.IData.UserRelation;

namespace Spoogr.Data.Sql.UserRelation
{
    public class UserRelationRepository: IUserRelationRepository
    {
        private readonly SpoogrDbContext _context;

        public UserRelationRepository(SpoogrDbContext context)
        {
            _context = context;
        }

        public async Task<int> AddUserRelation(Domain.UserRelation.UserRelation userRelation)
        {
            var userRelationDAO = new DAO.UserRelation
            {
                RelatedUserId = userRelation.RelatedUserId,
                RelatingUserId = userRelation.RelatingUserId,
                FollowDate = userRelation.FollowDate,
                BlockDate = userRelation.BlockDate,
                Follow = userRelation.Follow,
                Block = userRelation.Block
            };
            await _context.AddAsync(userRelationDAO);
            await _context.SaveChangesAsync();
            return userRelationDAO.UserRelationId;
        }

        public async Task<Domain.UserRelation.UserRelation> GetUserRelation(int userRelationId)
        {
            var userRelation = await _context.UserRelation.FirstOrDefaultAsync(x => x.UserRelationId == userRelationId);
            return new Domain.UserRelation.UserRelation(userRelation.UserRelationId,
                userRelation.RelatedUserId,
                userRelation.RelatingUserId,
                userRelation.FollowDate,
                userRelation.BlockDate,
                userRelation.Follow,
                userRelation.Block
            );
        }

        // nie ma edita
        
        public async Task DeleteUserRelation(int userRelationId)
        {
            var userRelation = await _context.UserRelation.FirstOrDefaultAsync(x => x.UserRelationId == userRelationId);

            _context.UserRelation.Remove(userRelation);

            await _context.SaveChangesAsync();
        }
    }
}