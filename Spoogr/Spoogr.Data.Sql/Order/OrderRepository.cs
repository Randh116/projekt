﻿using System.Linq;
using System.Threading.Tasks;
using Spoogr.IData.Order;
using Microsoft.EntityFrameworkCore;


namespace Spoogr.Data.Sql.Order
{
    public class OrderRepository: IOrderRepository
    {
        private readonly SpoogrDbContext _context;

        public OrderRepository(SpoogrDbContext context)
        {
            _context = context;
        }

        public async Task<int> AddOrder(Domain.Order.Order order)
        {
            var orderDAO = new DAO.Order
            {
                UserId = order.UserId,
                OrderDate = order.OrderDate,
                PaymentDate = order.PaymentDate,
                OrderItemsCount = order.OrderItemsCount,
                Price = order.Price
            };
            await _context.AddAsync(orderDAO);
            await _context.SaveChangesAsync();
            return orderDAO.OrderId;
        }

        public async Task<Domain.Order.Order> GetOrder(int orderId)
        {
            var order = await _context.Order.FirstOrDefaultAsync(x => x.OrderId == orderId);
            return new Domain.Order.Order(order.OrderId,
                order.UserId,
                order.OrderDate,
                order.PaymentDate,
                order.OrderItemsCount,
                order.Price
            );
        }
        
        // nie ma edita

        public async Task DeleteOrder(int orderId)
        {
            var order = await _context.Order.FirstOrDefaultAsync(x => x.OrderId == orderId);

            _context.Order.Remove(order);

            await _context.SaveChangesAsync();
        }
    }
}