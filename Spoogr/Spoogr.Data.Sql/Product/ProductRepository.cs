﻿using System.Linq;
using System.Threading.Tasks;
using Spoogr.IData.Product;
using Microsoft.EntityFrameworkCore;

namespace Spoogr.Data.Sql.Product
{
    public class ProductRepository: IProductRepository
    {
        private readonly SpoogrDbContext _context;

        public ProductRepository(SpoogrDbContext context)
        {
            _context = context;
        }

        public async Task<int> AddProduct(Domain.Product.Product product)
        {
            var productDAO = new DAO.Product
            {
                UserId = product.UserId,
                CommentsCount = product.CommentsCount,
                CreationDate = product.CreationDate,
                IsActiveProduct = product.IsActiveProduct,
                IsBannedProduct = product.IsBannedProduct,
                Price = product.Price,
                ProductDescription = product.ProductDescription,
                ProductHref = product.ProductHref,
                ProductName = product.ProductName,
            };
            await _context.AddAsync(productDAO);
            await _context.SaveChangesAsync();
            return productDAO.ProductId;
        }

        public async Task<Domain.Product.Product> GetProduct(int productId)
        {
            var product = await _context.Product.FirstOrDefaultAsync(x => x.ProductId == productId);
            return new Domain.Product.Product(product.ProductId,
                product.UserId,
                product.ProductName,
                product.IsActiveProduct,
                product.IsBannedProduct,
                product.CreationDate,
                product.EditionDate,
                product.CommentsCount,
                product.Price,
                product.ProductDescription,
                product.ProductHref
            );
        }

        public async Task<Domain.Product.Product> GetProduct(string productName)
        {
            var product = await _context.Product.FirstOrDefaultAsync(x => x.ProductName == productName);
            return new Domain.Product.Product(product.ProductId,
                product.UserId,
                product.ProductName,
                product.IsActiveProduct,
                product.IsBannedProduct,
                product.CreationDate,
                product.EditionDate,
                product.CommentsCount,
                product.Price,
                product.ProductDescription,
                product.ProductHref
            );
        }

        public async Task EditProduct(Domain.Product.Product product)
        {
            var editProduct = await _context.Product.FirstOrDefaultAsync(x => x.ProductId == product.ProductId);
            editProduct.ProductName = product.ProductName;
            editProduct.ProductDescription = product.ProductDescription;
            editProduct.ProductHref = product.ProductHref;
            editProduct.Price = product.Price;
            editProduct.EditionDate = product.EditionDate;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteProduct(int productId)
        {
            var product = await _context.Product.FirstOrDefaultAsync(x => x.ProductId == productId);

            _context.Product.Remove(product);

            await _context.SaveChangesAsync();
        }
    }
}