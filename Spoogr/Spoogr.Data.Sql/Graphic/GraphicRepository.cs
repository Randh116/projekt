﻿using System.Linq;
using System.Threading.Tasks;
using Spoogr.IData.Graphic;
using Microsoft.EntityFrameworkCore;
using Spoogr.Data.Sql.Graphic;

namespace Spoogr.Data.Sql.Graphic
{
    public class GraphicRepository: IGraphicRepository
    {
        private readonly SpoogrDbContext _context;

        public GraphicRepository(SpoogrDbContext context)
        {
            _context = context;
        }

        public async Task<int> AddGraphic(Domain.Graphic.Graphic graphic)
        {
            var graphicDAO = new DAO.Graphic
            {
                UserId = graphic.UserId,
                GraphicDescription = graphic.GraphicDescription,
                GraphicHref = graphic.GraphicHref,
                Price = graphic.Price,
                CreationDate = graphic.CreationDate,
                EditionDate = graphic.EditionDate,
                LikesCount = graphic.LikesCount,
                CommentsCount = graphic.CommentsCount,
                IsActivePost = graphic.IsActivePost,
                IsBannedPost = graphic.IsBannedPost
            };
            await _context.AddAsync(graphicDAO);
            await _context.SaveChangesAsync();
            return graphicDAO.GraphicId;
        }

        public async Task<Domain.Graphic.Graphic> GetGraphic(int graphicId)
        {
            var graphic = await _context.Graphic.FirstOrDefaultAsync(x => x.GraphicId == graphicId);
            return new Domain.Graphic.Graphic(graphic.GraphicId,
                graphic.UserId,
                graphic.GraphicDescription,
                graphic.GraphicHref,
                graphic.Price,
                graphic.CreationDate,
                graphic.EditionDate,
                graphic.LikesCount,
                graphic.CommentsCount,
                graphic.IsBannedPost,
                graphic.IsActivePost
            );
            
        }

        public async Task EditGraphic(Domain.Graphic.Graphic graphic)
        {
            var editGraphic = await _context.Graphic.FirstOrDefaultAsync(x => x.GraphicId == graphic.GraphicId);
            editGraphic.GraphicDescription = graphic.GraphicDescription;
            editGraphic.GraphicHref = graphic.GraphicHref;
            editGraphic.Price = graphic.Price;
            editGraphic.EditionDate = graphic.EditionDate;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteGraphic(int graphicId)
        {
            var graphic = await _context.Graphic.FirstOrDefaultAsync(x => x.GraphicId == graphicId);

            _context.Graphic.Remove(graphic);

            await _context.SaveChangesAsync();
        }
    }
}