﻿using System.Linq;
using System.Threading.Tasks;
using Spoogr.IData.Category;
using Microsoft.EntityFrameworkCore;
using Spoogr.Data.Sql.Category;

namespace Spoogr.Data.Sql.Category
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly SpoogrDbContext _context;

        public CategoryRepository(SpoogrDbContext context)
        {
            _context = context;
        }

        public async Task<int> AddCategory(Domain.Category.Category category)
        {
            var categoryDAO = new DAO.Category
            {
                CategoryName = category.CategoryName,
                ImageHref = category.ImageHref
            };
            await _context.AddAsync(categoryDAO);
            await _context.SaveChangesAsync();
            return categoryDAO.CategoryId;
        }

        public async Task<Domain.Category.Category> GetCategory(int categoryId)
        {
            var category = await _context.Category.FirstOrDefaultAsync(x => x.CategoryId == categoryId);
            return new Domain.Category.Category(category.CategoryId,
                category.CategoryName,
                category.ImageHref);
        }

        public async Task<Domain.Category.Category> GetCategory(string categoryName)
        {
            var category = await _context.Category.FirstOrDefaultAsync(x => x.CategoryName == categoryName);
            return new Domain.Category.Category(category.CategoryId,
                category.CategoryName,
                category.ImageHref);
        }

        public async Task EditCategory(Domain.Category.Category category)
        {
            var editCategory = await _context.Category.FirstOrDefaultAsync(x => x.CategoryId == category.CategoryId);
            editCategory.CategoryName = category.CategoryName;
            editCategory.ImageHref = category.ImageHref;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteCategory(int categoryId)
        {
            var category = await _context.Category.FirstOrDefaultAsync(x => x.CategoryId == categoryId);

            _context.Category.Remove(category);

            await _context.SaveChangesAsync();
        }
    }
}