﻿using System;

namespace Spoogr.Data.Sql.DAO
{
    public class Preference
    {
        public int PreferenceId { get; set; }
        public int UserId { get; set; }
        public int GraphicId { get; set; }
        public bool Like { get; set; }
        public DateTime LikeDate { get; set; }
        
        public virtual User User { get; set; }
        public virtual Graphic Graphic { get; set; }
    }
}