﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace Spoogr.Data.Sql.DAO
{
    public class PreparedProduct
    {

        public int PreparedProductId { get; set; }
        public int UserId { get; set; }
        public int GraphicId { get; set; }
        public int ProductId { get; set; }
        public int OrderId { get; set; }
        public int Price { get; set; }
        
        public virtual User User { get; set; }
        public virtual Graphic Graphic { get; set; }
        public virtual Product Product { get; set; }
        public virtual Order Order { get; set; }
    }
}