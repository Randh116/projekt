﻿using System;
using System.Collections.Generic;

namespace Spoogr.Data.Sql.DAO
{
    public class Product
    {
        public Product()
        {
            PreparedProducts = new List<PreparedProduct>();
            ProductCategories = new List<ProductCategory>();
        }
        
        public int ProductId { get; set; }
        public int UserId { get; set; }
        public string ProductName { get; set; }
        public bool IsActiveProduct { get; set; }
        public bool IsBannedProduct { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime EditionDate { get; set; }
        public int CommentsCount { get; set; }
        public int Price { get; set; }
        public string ProductDescription { get; set; }
        public string ProductHref { get; set; }
        
        public virtual User User { get; set; }
        public virtual ICollection<PreparedProduct> PreparedProducts { get; set; }
        public virtual ICollection<ProductCategory> ProductCategories { get; set; }
    }
}