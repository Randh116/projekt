﻿using System;
using System.Collections.Generic;

namespace Spoogr.Data.Sql.DAO
{
    public class Graphic
    {
        public Graphic()
        {
            Comments = new List<Comment>();
            Preferences = new List<Preference>();
            PreparedProducts = new List<PreparedProduct>();
        }
        
        public int GraphicId { get; set; }
        public int UserId { get; set; }
        public string GraphicDescription { get; set; }
        public string GraphicHref { get; set; }
        public int Price { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime EditionDate { get; set; }
        public int LikesCount { get; set; }
        public int CommentsCount { get; set; }
        public bool IsBannedPost { get; set; }
        public bool IsActivePost { get; set; }
        
        public virtual User User { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Preference> Preferences { get; set; }
        public virtual ICollection<PreparedProduct> PreparedProducts { get; set; }
    }
}