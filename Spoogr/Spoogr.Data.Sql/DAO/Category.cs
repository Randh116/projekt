﻿using System.Collections.Generic;

namespace Spoogr.Data.Sql.DAO
{
    //klasa (często nazywana encją) oddająca strukturę tabeli w bazie danych
    //wraz z relacjami z innymi encji/tabel
    public class Category
    {
        public Category()
        {
            ProductCategories = new List<ProductCategory>();
        }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string ImageHref { get; set; }

        public virtual ICollection<ProductCategory> ProductCategories { get; set; }
    }
}