﻿using System;
using System.Collections.Generic;

namespace Spoogr.Data.Sql.DAO
{
    public class Order
    {
        public Order()
        {
            PreparedProducts = new List<PreparedProduct>();
        }
        
        public int OrderId { get; set; }
        public int UserId { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? PaymentDate { get; set; }
        public int OrderItemsCount { get; set; }
        public int Price { get; set; }
        
        public virtual User User { get; set; }
        public virtual ICollection<PreparedProduct> PreparedProducts { get; set; }
    }
}