﻿namespace Spoogr.Data.Sql.DAO
{
    // ostatecznie trzeba będzie to dodać
    // relacje:
    // - wiele orderitem do jednego orderu - zeby mozna bylo wprowadzac ilosc prepareditem skladajacych sie na orderitem (quantity)
    // - wiele orderitem do jednego prepareditem - czyli ze prepareditem mozna kilkakrotnie wykorzystac
    
    public class OrderItem
    {
        public int OrderItemId { get; set; }
        public int OrderId { get; set; }
        public int PreparedProductId { get; set; }
        public int Quantity { get; set; }
        
        public virtual Order Order { get; set; }
        public virtual PreparedProduct PreparedProduct { get; set; }
    }
}