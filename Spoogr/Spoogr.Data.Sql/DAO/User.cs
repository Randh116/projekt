﻿using System;
using System.Collections.Generic;
using Spoogr.Common.Enums;

namespace Spoogr.Data.Sql.DAO
{
    public class User
    {
        public User()
        {
            Preferences = new List<Preference>();
            Graphics = new List<Graphic>();
            Products = new List<Product>();
            RelatingUsers = new List<UserRelation>();
            RelatedUsers = new List<UserRelation>();
            Comments = new List<Comment>();
            PreparedProducts = new List<PreparedProduct>();
            Orders = new List<Order>();
        }
        
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime EditionDate { get; set; }
        public DateTime BirthDate { get; set; }
        public Gender Gender { get; set; }
        public bool IsBannedUser { get; set; }
        public bool IsActiveUser { get; set; }
        public int GraphicsCount { get; set; }
        public int FollowersCount { get; set; }
        public int FollowingCount { get; set; }
        public string AccountDescription { get; set; }
        public string IconHref { get; set; }

        public virtual ICollection<Preference> Preferences { get; set; }
        public virtual ICollection<Graphic> Graphics { get; set; }
        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<UserRelation> RelatingUsers { get; set; }
        public virtual ICollection<UserRelation> RelatedUsers { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<PreparedProduct> PreparedProducts { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }
}