﻿using System;
using System.Threading.Tasks;
using Spoogr.IData.Product;
using Spoogr.IServices.Requests;
using Spoogr.IServices.Product;
using Spoogr.Services.User;
using Moq;
using Spoogr.Services.Product;
using Xunit;

namespace Spoogr.Services.Test.Product
{
    public class ProductServiceTest
    {
        private readonly IProductService _productService;
        private readonly Mock<IProductRepository> _productRepositoryMock;

        public ProductServiceTest()
        {
            _productRepositoryMock = new Mock<IProductRepository>();
            _productService = new ProductService(_productRepositoryMock.Object);
        }

        [Fact]
        public async Task CreateProduct_Returns_Correct_Response()
        {
            var product = new CreateProduct
            {
                Price = 1000,
                ProductName = "ProductName",
                UserId = 1
            };

            int expectedResult = 0;
            _productRepositoryMock.Setup(x => x.AddProduct(
                    new Domain.Product.Product(
                        product.UserId,
                        product.ProductName,
                        product.Price)))
                .Returns(Task.FromResult(expectedResult));
            
            //Act
            var result = await _productService.CreateProduct(product);
            
            //Assert
            Assert.IsType<Domain.Product.Product>(result);
            Assert.NotNull(result);
            Assert.Equal(expectedResult, result.ProductId);
        }

    }
}