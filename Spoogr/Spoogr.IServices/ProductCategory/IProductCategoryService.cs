﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Spoogr.IServices.Requests;

namespace Spoogr.IServices.ProductCategory
{
    public interface IProductCategoryService
    {
        Task<Spoogr.Domain.ProductCategory.ProductCategory>
            GetProductCategoryByProductCategoryId(int productCategoryId);

        Task<Spoogr.Domain.ProductCategory.ProductCategory> CreateProductCategory(
            CreateProductCategory createProductCategory);

        Task DeleteProductCategory(int productCategoryId);
    }
}