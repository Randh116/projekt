﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Spoogr.IServices.Requests;

namespace Spoogr.IServices.Preference
{
    public interface IPreferenceService
    {
        Task<Spoogr.Domain.Preference.Preference> GetPreferenceByPreferenceId(int preferenceId);
        Task<Spoogr.Domain.Preference.Preference> CreatePreference(CreatePreference createPreference);
        Task DeletePreference(int preferenceId);
    }
}