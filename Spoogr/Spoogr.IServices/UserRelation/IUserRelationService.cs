﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Spoogr.IServices.Requests;

namespace Spoogr.IServices.UserRelation
{
    public interface IUserRelationService
    {
        Task<Spoogr.Domain.UserRelation.UserRelation> GetUserRelationByUserRelationId(int userRelationId);
        Task<Spoogr.Domain.UserRelation.UserRelation> CreateUserRelation(CreateUserRelation createUserRelation);
        Task DeleteUserRelation(int userRelationId);
    }
}