﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Spoogr.IServices.Requests;

namespace Spoogr.IServices.User
{
    public interface IUserService
    {
        Task<Spoogr.Domain.User.User> GetUserByUserId(int userId);
        Task<Spoogr.Domain.User.User> GetUserByUserName(string userName);
        Task<Spoogr.Domain.User.User> CreateUser(CreateUser createUser);
        Task EditUser(EditUser createUser, int userId);
        Task DeleteUser(int userId);
    }
}