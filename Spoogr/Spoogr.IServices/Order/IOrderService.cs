﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Spoogr.IServices.Requests;

namespace Spoogr.IServices.Order
{
    public interface IOrderService
    {
        Task<Spoogr.Domain.Order.Order> GetOrderByOrderId(int orderId);
        Task<Spoogr.Domain.Order.Order> CreateOrder(CreateOrder createOrder);
        Task DeleteOrder(int orderId);
    }
}