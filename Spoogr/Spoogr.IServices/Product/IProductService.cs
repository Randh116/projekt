﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Connections;
using Spoogr.IServices.Requests;

namespace Spoogr.IServices.Product
{
    public interface IProductService
    {
        Task<Spoogr.Domain.Product.Product> GetProductByProductId(int productId);
        Task<Spoogr.Domain.Product.Product> GetProductByProductName(string productName);
        Task<Spoogr.Domain.Product.Product> CreateProduct(CreateProduct createProduct);
        Task EditProduct(EditProduct createProduct, int productId);
        Task DeleteProduct(int productId);
    }
}