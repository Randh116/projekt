﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Spoogr.IServices.Requests;

namespace Spoogr.IServices.Category
{
    public interface ICategoryService
    {
        Task<Spoogr.Domain.Category.Category> GetCategoryByCategoryId(int categoryId);
        Task<Spoogr.Domain.Category.Category> GetCategoryByCategoryName(string categoryName);
        Task<Spoogr.Domain.Category.Category> CreateCategory(CreateCategory createCategory);
        Task EditCategory(EditCategory createCategory, int categoryId);
        Task DeleteCategory(int categoryId);
    }
}