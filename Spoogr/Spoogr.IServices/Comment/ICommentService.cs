﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Spoogr.IServices.Requests;

namespace Spoogr.IServices.Comment
{
    public interface ICommentService
    {
        Task<Spoogr.Domain.Comment.Comment> GetCommentByCommentId(int commentId);
        Task<Spoogr.Domain.Comment.Comment> CreateComment(CreateComment createComment);
        Task EditComment(EditComment createComment, int commentId);
        Task DeleteComment(int commentId);
    }
}