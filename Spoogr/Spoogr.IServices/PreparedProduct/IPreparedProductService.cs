﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Spoogr.IServices.Requests;

namespace Spoogr.IServices.PreparedProduct
{
    public interface IPreparedProductService
    {
        Task<Spoogr.Domain.PreparedProduct.PreparedProduct>
            GetPreparedProductByPreparedProductId(int preparedProductId);
        
        Task<Spoogr.Domain.PreparedProduct.PreparedProduct> CreatePreparedProduct(
            CreatePreparedProduct createPreparedProduct);

        Task DeletePreparedProduct(int preparedProductId);
    }
}