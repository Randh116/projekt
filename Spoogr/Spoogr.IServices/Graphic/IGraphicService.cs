﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Connections;
using Spoogr.IServices.Requests;

namespace Spoogr.IServices.Graphic
{
    public interface IGraphicService
    {
        Task<Spoogr.Domain.Graphic.Graphic> GetGraphicByGraphicId(int graphicId);
        Task<Spoogr.Domain.Graphic.Graphic> CreateGraphic(CreateGraphic createGraphic);
        Task EditGraphic(EditGraphic createGraphic, int graphicId);
        Task DeleteGraphic(int graphicId);
    }
}