﻿using System;
using System.Reflection.Emit;
using Spoogr.Common.Enums;

namespace Spoogr.IServices.Requests
{
    public class CreatePreparedProduct
    {
        public int UserId { get; set; }
        public int GraphicId { get; set; }
        public int ProductId { get; set; }
        public int OrderId { get; set; }
        public int Price { get; set; }
    }
}