﻿namespace Spoogr.IServices.Requests
{
    public class EditComment
    {
        public string CommentBody { get; set; }
    }
}