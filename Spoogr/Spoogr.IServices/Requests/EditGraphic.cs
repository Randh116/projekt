﻿using System;

namespace Spoogr.IServices.Requests
{
    public class EditGraphic
    {
        public string GraphicDescription { get; set; }
        public string GraphicHref { get; set; }
        public int Price { get; set; }
    }
}