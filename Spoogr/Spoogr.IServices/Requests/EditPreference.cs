﻿using System;

namespace Spoogr.IServices.Requests
{
    public class EditPreference
    {
        public int UserId { get; set; }
        public int GraphicId { get; set; }
        public bool Like { get; set; }
        public DateTime LikeDate { get; set; }
    }
}