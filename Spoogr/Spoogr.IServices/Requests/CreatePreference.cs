﻿using System;
using System.Reflection.Emit;
using Spoogr.Common.Enums;

namespace Spoogr.IServices.Requests
{
    public class CreatePreference
    {
        public int UserId { get; set; }
        public int GraphicId { get; set; }
    }
}