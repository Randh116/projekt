﻿using System;
using System.Reflection.Emit;
using Spoogr.Common.Enums;

namespace Spoogr.IServices.Requests
{
    public class CreateGraphic
    {
        public int UserId { get; set; }
        public string GraphicDescription { get; set; }
        public int Price { get; set; }
    }
}