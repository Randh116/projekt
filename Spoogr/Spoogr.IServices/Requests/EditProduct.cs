﻿using System;

namespace Spoogr.IServices.Requests
{
    public class EditProduct
    {
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public string ProductHref { get; set; }
        public int Price { get; set; }
    }
}