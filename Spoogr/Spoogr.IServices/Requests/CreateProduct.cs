﻿using System;
using System.Reflection.Emit;
using Spoogr.Common.Enums;

namespace Spoogr.IServices.Requests
{
    public class CreateProduct
    {
        public int UserId { get; set; }
        public string ProductName { get; set; }
        public int Price { get; set; }
    }
}