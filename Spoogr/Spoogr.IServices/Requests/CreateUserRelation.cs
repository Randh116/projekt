﻿using System;
using System.Reflection.Emit;
using Spoogr.Common.Enums;

namespace Spoogr.IServices.Requests
{
    public class CreateUserRelation
    {
        public int RelatedUserId { get; set; }
        public int RelatingUserId { get; set; }
    }
}