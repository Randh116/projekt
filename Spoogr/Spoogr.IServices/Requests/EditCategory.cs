﻿namespace Spoogr.IServices.Requests
{
    public class EditCategory
    {
        public string CategoryName { get; set; }
        public string ImageHref { get; set; }
    }
}