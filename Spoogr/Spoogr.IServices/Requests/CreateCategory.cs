﻿using System;
using System.Reflection.Emit;
using Spoogr.Common.Enums;

namespace Spoogr.IServices.Requests
{
    public class CreateCategory
    {
        public string CategoryName { get; set; }
    }
}