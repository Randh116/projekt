﻿using System;
using System.Reflection.Emit;
using Spoogr.Common.Enums;

namespace Spoogr.IServices.Requests
{
    public class CreateProductCategory
    {
        public int ProductId { get; set; }
        public int CategoryId { get; set; }
    }
}