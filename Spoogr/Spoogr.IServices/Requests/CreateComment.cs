﻿using System;
using System.Reflection.Emit;
using Spoogr.Common.Enums;

namespace Spoogr.IServices.Requests
{
    public class CreateComment
    {
        public int GraphicId { get; set; }
        public int UserId { get; set; }
    }
}