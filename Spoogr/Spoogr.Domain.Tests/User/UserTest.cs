﻿using System;
using Spoogr.Common.Enums;
using Spoogr.Domain.DomainExceptions;
using Xunit;

namespace Spoogr.Domain.Test.User
{
    public class UserTest
    {
        public UserTest()
        {
            //Arrange
            //Act
            //Assert
        }

        [Fact]
        public void CreateUser_Returns_Throws_InvalidBirthDateException()
        {
            Assert.Throws<InvalidBirthDateException>
            (() => new Domain.User.User("Name",
                "Email",
                Gender.Male,
                DateTime.UtcNow.AddHours(1)));
        }

        [Fact]
        public void CreateUser_Returns_Correct_Reponse()
        {
            var user = new Domain.User.User("Name", "Email", Gender.Male, DateTime.UtcNow);
            
            Assert.Equal(Gender.Male, user.Gender);
            Assert.Equal("Email", user.Email);
            Assert.Equal("Name", user.UserName);
        }
    }
}