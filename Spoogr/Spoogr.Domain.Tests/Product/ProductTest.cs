﻿using System;
using Xunit;

namespace Spoogr.Domain.Test.Product
{
    public class ProductTest
    {
        public ProductTest()
        {
            
        }

        [Fact]
        public void CreateProduct_Returns_Correct_Response()
        {
            var product = new Domain.Product.Product(1, "ProductName", 1000);
            
            Assert.Equal(1, product.UserId);
            Assert.Equal("ProductName", product.ProductName);
            Assert.Equal(1000, product.Price);
        }
    }
}