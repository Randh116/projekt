﻿using System;
using System.Data.Common;
using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Converters;
using Spoogr.Common.Enums;
using Spoogr.Domain.DomainExceptions;

namespace Spoogr.Domain.Product
{
    public class Product
    {
        public int ProductId { get; set; }
        public int UserId { get; private set; }
        public string ProductName { get; private set; }
        public bool IsActiveProduct { get; private set; }
        public bool IsBannedProduct { get; private set; }
        public DateTime CreationDate { get; private set; }
        public DateTime EditionDate { get; private set; }
        public int CommentsCount { get; private set; }
        public int Price { get; private set; }
        public string ProductDescription { get; private set; }
        public string ProductHref { get; private set; }

        public Product(int productId, int userId, string productName, bool isActiveProduct, bool isBannedProduct,
            DateTime creationDate, DateTime editionDate, int commentsCount, int price, string productDescription,
            string productHref)
        {
            ProductId = productId;
            UserId = userId;
            ProductName = productName;
            IsActiveProduct = isActiveProduct;
            IsBannedProduct = isBannedProduct;
            CreationDate = creationDate;
            EditionDate = editionDate;
            CommentsCount = commentsCount;
            Price = price;
            ProductDescription = productDescription;
            ProductHref = productHref;
        }

        public Product(int userId, string productName, int price)
        {
            UserId = userId;
            ProductName = productName;
            IsActiveProduct = true;
            IsBannedProduct = false;
            CreationDate = DateTime.UtcNow;
            EditionDate = DateTime.UtcNow;
            CommentsCount = 0;
            Price = price;
            ProductDescription = "test product";
        }

        public void EditProduct(string productName, string productDescription, string productHref, int price)
        {
            ProductName = productName;
            ProductDescription = productDescription;
            ProductHref = productHref;
            Price = price;
            EditionDate = DateTime.UtcNow;
        }
    }
}