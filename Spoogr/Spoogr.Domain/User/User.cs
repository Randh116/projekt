﻿using System;
using System.Data.Common;
using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Converters;
using Spoogr.Common.Enums;
using Spoogr.Domain.DomainExceptions;

namespace Spoogr.Domain.User
{
    public class User
    {
        public int UserId { get; set; }
        public string UserName { get; private set; }
        public string Email { get; private set; }
        public DateTime RegistrationDate { get; private set; }
        public DateTime EditionDate { get; private set; }
        public DateTime BirthDate { get; private set; }
        public Gender Gender { get; private set; }
        public bool IsBannedUser { get; private set; }
        public bool IsActiveUser { get; private set; }
        public int GraphicsCount { get; private set; }
        public int FollowersCount { get; private set; }
        public int FollowingCount { get; private set; }
        public string AccountDescription { get; private set; }
        public string IconHref { get; private set; }
        
        public User(int id, string userName, string email, DateTime registrationDate, DateTime editionDate, DateTime birthDate, Gender gender, bool isBannedUser, bool isActiveUser, int graphicsCount, int followersCount, int followingCount, string accountDescription, string iconHref)
        {
            UserId = id;
            UserName = userName;
            Email = email;
            RegistrationDate = registrationDate;
            EditionDate = editionDate;
            BirthDate = birthDate;
            Gender = gender;
            IsBannedUser = isBannedUser;
            IsActiveUser = isActiveUser;
            GraphicsCount = graphicsCount;
            FollowersCount = followersCount;
            FollowingCount = followingCount;
            AccountDescription = accountDescription;
            IconHref = iconHref;
        }

        public User(string userName, string email, Gender gender, DateTime birthDate)
        {
            if (birthDate >= DateTime.UtcNow)
                throw new InvalidBirthDateException(birthDate);
            
            UserName = userName;
            Email = email;
            Gender = gender;
            BirthDate = birthDate;
            RegistrationDate = DateTime.UtcNow;
            EditionDate = DateTime.UtcNow;
            IsBannedUser = false;
            IsActiveUser = true;
            GraphicsCount = 0;
            FollowersCount = 0;
            FollowingCount = 0;
        }

        public void EditUser(string userName, string email, Gender gender, DateTime birthDate)
        {
            if (birthDate >= DateTime.UtcNow)
                throw new InvalidBirthDateException(birthDate);

            UserName = userName;
            Email = email;
            Gender = gender;
            BirthDate = birthDate;
            EditionDate = DateTime.UtcNow;
        }
    }
}