﻿namespace Spoogr.Domain.ProductCategory
{
    public class ProductCategory
    {
        public int ProductCategoryId { get; set; }
        public int ProductId { get; private set; }
        public int CategoryId { get; private set; }

        public ProductCategory(int productCategoryId, int productId, int categoryId)
        {
            ProductCategoryId = productCategoryId;
            ProductId = productId;
            CategoryId = categoryId;
        }

        public ProductCategory(int productId, int categoryId)
        {
            ProductId = productId;
            CategoryId = categoryId;
        }
    }
}