﻿using System;
using System.Data.Common;
using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Converters;
using Spoogr.Common.Enums;
using Spoogr.Domain.DomainExceptions;


namespace Spoogr.Domain.PreparedProduct
{
    public class PreparedProduct
    {
        public int PreparedProductId { get; set; }
        public int UserId { get; private set; }
        public int GraphicId { get; private set; }
        public int ProductId { get; private set; }
        public int OrderId { get; private set; }
        public int Price { get; private set; }

        public PreparedProduct(int preparedProductId, int userId, int graphicId, int productId, int orderId, int price)
        {
            PreparedProductId = preparedProductId;
            UserId = userId;
            GraphicId = graphicId;
            ProductId = productId;
            OrderId = orderId;
            Price = price;
        }
        
        public PreparedProduct(int userId, int graphicId, int productId, int orderId, int price)
        {
            UserId = userId;
            GraphicId = graphicId;
            ProductId = productId;
            OrderId = orderId;
            Price = price;
        }
    }
}