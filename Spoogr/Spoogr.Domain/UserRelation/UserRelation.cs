﻿using System;
using System.Data.Common;
using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Converters;
using Spoogr.Common.Enums;
using Spoogr.Domain.DomainExceptions;

namespace Spoogr.Domain.UserRelation
{
    public class UserRelation
    {
        public int UserRelationId { get; set; }
        public int RelatedUserId { get; set; }
        public int RelatingUserId { get; set; }
        public DateTime? FollowDate { get; private set; }
        public DateTime? BlockDate { get; private set; }
        public bool Follow { get; private set; }
        public bool Block { get; private set; }

        public UserRelation(int userRelationId, int relatedUserId, int relatingUserId, DateTime? followDate, DateTime? blockDate, bool follow, bool block)
        {
            UserRelationId = userRelationId;
            RelatedUserId = relatedUserId;
            RelatingUserId = relatingUserId;
            FollowDate = followDate;
            BlockDate = blockDate;
            Follow = follow;
            Block = block;
        }

        public UserRelation(int relatedUserId, int relatingUserId)
        {
            RelatedUserId = relatedUserId;
            RelatingUserId = relatingUserId;
            FollowDate = DateTime.UtcNow;
            BlockDate = null;
            Follow = true;
            Block = false;
        }
    }
}