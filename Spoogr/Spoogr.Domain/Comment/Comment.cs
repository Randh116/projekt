﻿using System;
using System.Data.Common;
using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Converters;
using Spoogr.Common.Enums;
using Spoogr.Domain.DomainExceptions;


namespace Spoogr.Domain.Comment
{
    public class Comment
    {
        public int CommentId { get; set; }
        public int? ParentCommentId { get; set; }
        public int GraphicId { get; private set; }
        public int UserId { get; private set; }
        public string CommentBody { get; private set; }
        public DateTime CommentDate { get; private set; }
        public bool IsActiveComment { get; private set; }
        public bool IsBannedComment { get; private set; }

        public Comment(int commentId, int? parentCommentId, int graphicId, int userId, string commentBody, DateTime commentDate, bool isActiveComment, bool isBannedComment)
        {
            CommentId = commentId;
            ParentCommentId = parentCommentId;
            GraphicId = graphicId;
            UserId = userId;
            CommentBody = commentBody;
            CommentDate = commentDate;
            IsActiveComment = isActiveComment;
            IsBannedComment = isBannedComment;
        }

        public Comment(int graphicId, int userId)
        {
            ParentCommentId = null;
            GraphicId = graphicId;
            UserId = userId;
            CommentBody = "test";
            CommentDate = DateTime.UtcNow;
            IsActiveComment = true;
            IsBannedComment = false;
        }
        
        // dodac editiondate
        public void EditComment(string commentBody)
        {
            CommentBody = commentBody;
        }
    }
}