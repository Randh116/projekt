﻿using System;
using System.Data.Common;
using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Converters;
using Spoogr.Common.Enums;
using Spoogr.Domain.DomainExceptions;

namespace Spoogr.Domain.Preference
{
    public class Preference
    {
        public int PreferenceId { get; set; }
        public int UserId { get; private set; }
        public int GraphicId { get; private set; }
        public bool Like { get; private set; }
        public DateTime LikeDate { get; private set; }

        public Preference(int preferenceId, int userId, int graphicId, bool like, DateTime likeDate)
        {
            PreferenceId = preferenceId;
            UserId = userId;
            GraphicId = graphicId;
            Like = like;
            LikeDate = likeDate;
        }

        public Preference(int userId, int graphicId)
        {
            UserId = userId;
            GraphicId = graphicId;
            Like = true;
            LikeDate = DateTime.UtcNow;
        }
    }
}