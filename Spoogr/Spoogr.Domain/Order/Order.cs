﻿using System;
using System.Data.Common;
using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Converters;
using Spoogr.Common.Enums;
using Spoogr.Domain.DomainExceptions;

namespace Spoogr.Domain.Order
{
    public class Order
    {
        public int OrderId { get; set; }
        public int UserId { get; private set; }
        public DateTime? OrderDate { get; private set; }
        public DateTime? PaymentDate { get; private set; }
        public int OrderItemsCount { get; private set; }
        public int Price { get; private set; }

        public Order(int orderId, int userId, DateTime? orderDate, DateTime? paymentDate, int orderItemsCount, int price)
        {
            OrderId = orderId;
            UserId = userId;
            OrderDate = orderDate;
            PaymentDate = paymentDate;
            OrderItemsCount = orderItemsCount;
            Price = price;
        }

        public Order(int userId)
        {
            UserId = userId;
            OrderDate = DateTime.UtcNow;
            PaymentDate = null;
        }
    }
}