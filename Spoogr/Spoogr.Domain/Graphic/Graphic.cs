﻿using System;
using System.Data.Common;
using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Converters;
using Spoogr.Common.Enums;
using Spoogr.Domain.DomainExceptions;

namespace Spoogr.Domain.Graphic
{
    public class Graphic
    {
        public int GraphicId { get; set; }
        public int UserId { get; private set; }
        public string GraphicDescription { get; private set; }
        public string GraphicHref { get; private set; }
        public int Price { get; private set; }
        public DateTime CreationDate { get; private set; }
        public DateTime EditionDate { get; private set; }
        public int LikesCount { get; private set; }
        public int CommentsCount { get; private set; }
        public bool IsBannedPost { get; private set; }
        public bool IsActivePost { get; private set; }
        
        // - dodawanie pelnymi parametrami
        // - dodawanie kilkoma podstawowymi parametrami (id, name, price)
        // - edytowanie pozycji w tabeli

        // trzeba przemodelowac i dodac pole 'graphicName'
        // trzeba nazwac IsActivePost na IsActiveGraphic - adekwatne nazwy
        
        public Graphic(int graphicId, int userId, string graphicDescription,
            string graphicHref, int price, DateTime creationDate, DateTime editionDate,
            int likesCount, int commentsCount, bool isBannedPost, bool isActivePost)
        {
            GraphicId = graphicId;
            UserId = userId;
            GraphicDescription = graphicDescription;
            GraphicHref = graphicHref;
            Price = price;
            CreationDate = creationDate;
            EditionDate = editionDate;
            LikesCount = likesCount;
            CommentsCount = commentsCount;
            IsBannedPost = isBannedPost;
            IsActivePost = isActivePost;
        }
        
        // tymczasowo zrobione po graphicDescription, docelowo ma byc graphicName
        public Graphic(int userId, string graphicDescription, int price)
        {
            UserId = userId;
            GraphicDescription = graphicDescription;
            Price = price;
            CreationDate = DateTime.UtcNow;
            EditionDate = DateTime.UtcNow;
            LikesCount = 0;
            CommentsCount = 0;
            IsActivePost = true;
            IsBannedPost = false;
        }

        public void EditGraphic(string graphicDescription, string graphicHref, int price)
        {
            GraphicDescription = graphicDescription;
            GraphicHref = graphicHref;
            Price = price;
            EditionDate = DateTime.UtcNow;
        }
    }
}