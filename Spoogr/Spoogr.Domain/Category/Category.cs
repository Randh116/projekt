﻿using System;
using System.Data.Common;
using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Converters;
using Spoogr.Common.Enums;
using Spoogr.Domain.DomainExceptions;

namespace Spoogr.Domain.Category
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; private set; }
        public string ImageHref { get; private set; }

        public Category(int categoryId, string categoryName, string imageHref)
        {
            CategoryId = categoryId;
            CategoryName = categoryName;
            ImageHref = imageHref;
        }

        public Category(string categoryName)
        {
            CategoryName = categoryName;
        }

        public void EditCategory(string categoryName, string imageHref)
        {
            CategoryName = categoryName;
            ImageHref = imageHref;
        }
    }
}