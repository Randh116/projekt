﻿using System;
using System.Threading.Tasks;
using Spoogr.Common.Enums;
using Spoogr.Data.Sql.User;
using Spoogr.IData.User;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Xunit;

namespace Spoogr.Data.Sql.Tests.User
{
    public class UserRepositoryTest
    {
        public IConfiguration Configuration { get; }
        private SpoogrDbContext _context;
        private IUserRepository _userRepository;

        // dzialamy na prawdziwej bazie lokalnej
        public UserRepositoryTest()
        {
            var optionsBuilder = new DbContextOptionsBuilder<SpoogrDbContext>();
            optionsBuilder.UseNpgsql("server=localhost;userid=postgres;pwd=Rafal123#;port=5432;database=spoogr_db;");
            _context = new SpoogrDbContext(optionsBuilder.Options);
            _context.Database.EnsureDeleted();
            _context.Database.EnsureCreated();
            _userRepository = new UserRepository(_context);
        }
        
        // integracyjne
        [Fact]
        public async Task AddUser_Returns_Correct_Response()
        {
            var user = new Domain.User.User("Name", "Email", Gender.Male, DateTime.UtcNow);
            var userId = await _userRepository.AddUser(user);

            var createdUser = await _context.User.FirstOrDefaultAsync(x => x.UserId == userId);
            Assert.NotNull(createdUser);

            // trzeba po sobie posprzatac
            _context.User.Remove(createdUser);
            await _context.SaveChangesAsync();
        }
        
    }
}