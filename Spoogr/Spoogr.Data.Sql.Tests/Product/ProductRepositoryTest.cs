﻿using System;
using System.Threading.Tasks;
using Spoogr.Common.Enums;
using Spoogr.Data.Sql.Product;
using Spoogr.IData.Product;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Spoogr.Data.Sql.User;
using Spoogr.IData.User;
using Xunit;

namespace Spoogr.Data.Sql.Tests.Product
{
    public class ProductRepositoryTest
    {
        public IConfiguration Configuration { get; }
        private SpoogrDbContext _context;
        private IUserRepository _userRepository;
        private IProductRepository _productRepository;

        public ProductRepositoryTest()
        {
            var optionsBuilder = new DbContextOptionsBuilder<SpoogrDbContext>();
            optionsBuilder.UseNpgsql("server=localhost;userid=postgres;pwd=Rafal123#;port=5432;database=spoogr_db;");
            _context = new SpoogrDbContext(optionsBuilder.Options);
            _context.Database.EnsureDeleted();
            _context.Database.EnsureCreated();
            _userRepository = new UserRepository(_context);
            _productRepository = new ProductRepository(_context);
        }
        
        // passed
        [Fact]
        public async Task AddProduct_with_AddUser_Returns_Correct_Response()
        {
            var user = new Domain.User.User("Name", "Email", Gender.Male, DateTime.UtcNow);
            var userId = await _userRepository.AddUser(user);

            var createdUser = await _context.User.FirstOrDefaultAsync(x => x.UserId == userId);
            Assert.NotNull(createdUser);
            
            var product = new Domain.Product.Product(1, "ProductName", 1000);
            var productId = await _productRepository.AddProduct(product);

            var createdProduct = await _context.Product.FirstOrDefaultAsync(x => x.ProductId == productId);
            Assert.NotNull(createdProduct);

            _context.Product.Remove(createdProduct);
            _context.User.Remove(createdUser);
            await _context.SaveChangesAsync();
        }
        
        // failed
        [Fact]
        public async Task AddProduct_Returns_Correct_Response()
        {
            var product = new Domain.Product.Product(1, "ProductName", 1000);
            var productId = await _productRepository.AddProduct(product);

            var createdProduct = await _context.Product.FirstOrDefaultAsync(x => x.ProductId == productId);
            Assert.NotNull(createdProduct);

            _context.Product.Remove(createdProduct);
            await _context.SaveChangesAsync();
        }
    }
}